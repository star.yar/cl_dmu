import os

import numpy as np
import torch
from torch.utils.data import Dataset, Sampler
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence

from phoneme_list import PHONEME_LIST, PHONEME_MAP
from utils import get_item


class WSJ(object):
    """ Load the WSJ speech dataset

        Ensure dump_path is path to directory containing
        all data files (.npy) provided on Kaggle.

        Example usage:
            loader = WSJ()
            trainX, trainY = loader.train
            assert(trainX.shape[0] == 24590)

    """

    def __init__(self, numpy_dump_path):
        self.dev_set = None
        self.train_set = None
        self.test_set = None
        self.dump_path = numpy_dump_path

    @property
    def dev(self):
        if self.dev_set is None:
            self.dev_set = WSJ.load_raw(self.dump_path, 'dev')
        return self.dev_set

    @property
    def train(self):
        if self.train_set is None:
            self.train_set = WSJ.load_raw(self.dump_path, 'train')
        return self.train_set

    @property
    def test(self):
        if self.test_set is None:
            self.test_set = (np.load(os.path.join(self.dump_path, 'test.npy'), encoding='bytes'), None)
        return self.test_set

    @staticmethod
    def load_raw(path, name):
        return (
            np.load(os.path.join(path, '{}.npy'.format(name)), encoding='bytes', allow_pickle=True),
            np.load(os.path.join(path, '{}_merged_labels.npy'.format(name)), encoding='bytes', allow_pickle=True)
        )


class PhonemesDataset(Dataset):
    """Face dataset for CMU kaggle2."""

    def __init__(self, data_path, mode='dev'):
        """
        Args:
            data_path (string): Directory with data.

        Possible data paths:
            wsj0_train.npy
            wsj0_train_merged_labels.npy
            wsj0_test.npy
            wsj0_dev.npy
            wsj0_dev_merged_labels.npy
        """

        self.all_utters, self.all_phonemes = WSJ.load_raw(data_path, mode)

    def __len__(self):
        return len(self.all_utters)

    def __getitem__(self, idx):
        seq, label = (self.all_utters[idx],
                      self.all_phonemes[idx])
        return (torch.tensor(seq),
                torch.tensor(label))


if __name__ == '__main__':
    # some tests

    # test dataset
    train_data = PhonemesDataset('/Users/star_yar/Downloads/kaggle3data.nosync')
    utter_i, phonemes_i = train_data[0]
    phoneme_decoding = [PHONEME_LIST[phoneme] for phoneme in phonemes_i]
    phoneme_mapping = [PHONEME_MAP[phoneme] for phoneme in phonemes_i]
    print('Phoneme sequence character representation:', ''.join(phoneme_mapping))
    print(f'Data len: {len(train_data)}')
