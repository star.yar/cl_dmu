import torch
import torch.nn.functional as F
import torch.nn as nn
from torch.nn.utils.rnn import pad_sequence, pack_padded_sequence, pad_packed_sequence
from torch.utils.data import BatchSampler, DataLoader

from data_utils import PhonemesDataset


class CustomNet(nn.Module):
    """
    Custom architecture
        - adapts for unknown input into fully connected block
    """

    def __init__(self, input_dim, output_dim, lstm_layers=8, hidden_size=16):
        """

        Args:
            phoneme_shape (int): the dimensionality of 1 time unit of recording
            output_dim (int): dim of output
        """
        super().__init__()

        self.input_dim = input_dim
        self.output_dim = output_dim
        self.hidden_size = hidden_size
        self.lstm_layers = lstm_layers

        self.lstm = nn.Sequential(
            nn.LSTM(self.input_dim, self.hidden_size, self.lstm_layers),
            nn.LSTM(self.hidden_size, self.hidden_size // 2, self.lstm_layers // 2),
        )
        self.linear = nn.Linear(self.hidden_size // 2, self.output_dim)

    def forward(self, x):
        lstm_out, _ = self.lstm(x)
        lstm_out, lens = pad_packed_sequence(lstm_out)
        t, batch_size, _ = lstm_out.shape
        linear_out = self.linear(lstm_out.view(-1, self.hidden_size))
        phoneme_scores = F.log_softmax(linear_out, dim=1)
        return phoneme_scores.view(t, batch_size, self.output_dim), lens


def init_weights(m):
    if type(m) == nn.Conv2d or type(m) == nn.Linear:
        nn.init.xavier_normal_(m.weight.data)


def get_trainable_parameters(model):
    return [p.numel() for p in model.parameters() if p.requires_grad]


def collate_fn(data):

    sorted_data = sorted(data, key=lambda x: x[0].shape[0], reverse=True)

    # seq_x = [x.view(x.shape[0], 1, x.shape[1]) for x, _ in sorted_data]
    # seq_y = [y.view(1, y.shape[0]) for _, y in sorted_data]
    seq_x = [x for x, _ in sorted_data]
    seq_y = [y for _, y in sorted_data]
    x_lens = [len(x) for x, _ in sorted_data]

    padded_x = pad_sequence(seq_x)
    # padded_y = pad_sequence(seq_y)

    packed_x = pack_padded_sequence(padded_x, x_lens)
    # packed_y = pack_padded_sequence(padded_y, [len(y) for _, y in sorted_data])

    return packed_x, seq_y


if __name__ == '__main__':
    from phoneme_list import PHONEME_MAP

    # create dataset
    train_data = PhonemesDataset('/Users/star_yar/Downloads/kaggle3data.nosync')
    utter_i, phonemes_i = train_data[0]

    # # code for viewing the distribution of recordings' lengths
    # import matplotlib.pyplot as plt
    # import seaborn as sns
    # lengths = [len(x) for x, _ in train_data]
    # sns.distplot(lengths)
    # plt.show()

    # create data loader
    loader = DataLoader(train_data, shuffle=False, batch_size=10, collate_fn=collate_fn)
    all_iter_data = [(_in, _tg) for _in, _tg in loader]
    _input, _target = all_iter_data[0]

    # process targets
    padded_target = pad_sequence(_target, batch_first=True)
    target_lengths = torch.tensor([len(x) for x in _target])
    model = CustomNet(40, len(PHONEME_MAP) + 1)
    output, input_lengths = model(_input)
    criterion = nn.CTCLoss()
    loss = criterion(output, padded_target, input_lengths, target_lengths)

    # TODO: build greedy word builder or use ctc decoder
