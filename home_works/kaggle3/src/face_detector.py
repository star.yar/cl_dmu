import numpy as np
import cv2 as cv
from configs import TRAIN_PATH
from matplotlib import pyplot as plt
import seaborn as sns

haar_path = '/anaconda3/pkgs/libopencv-3.4.2-h7c891bd_1/share/OpenCV'

# load img
img = cv.imread(f'{TRAIN_PATH}/0/0007_01.jpg')
gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
plt.imshow(gray)
plt.show()

# # by color distributions
# for color_layer, color in zip(img, ['r', 'g', 'b']):
#     sns.distplot(color_layer.flatten(), color=color)
# plt.show()
#
# # color hist normalisation
# hist, bins = np.histogram(gray.flatten(), 256, [0, 256])
# cdf = hist.cumsum()
# cdf_normalized = cdf * hist.max() / cdf.max()
# plt.plot(cdf_normalized, color='b')
# plt.hist(gray.flatten(), 256, [0, 256], color='r')
# plt.show()
#
# # normalize color hist
# cdf_m = np.ma.masked_equal(cdf, 0)
# cdf_m = (cdf_m - cdf_m.min()) * 255 / (cdf_m.max() - cdf_m.min())
# cdf = np.ma.filled(cdf_m, 0).astype('uint8')
# gray2 = cdf[img]
# plt.plot(cdf, color='b')
# plt.hist(gray2.flatten(), 256, [0, 256], color='r')
# plt.show()
#
# plt.imshow(gray2)
# plt.show()
#
# clahe = cv.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
# cl1 = clahe.apply(gray)
# plt.imshow(cl1)
# plt.show()

face_cascade = cv.CascadeClassifier(f'{haar_path}/haarcascades/haarcascade_frontalface_default.xml')
eye_cascade = cv.CascadeClassifier(f'{haar_path}/haarcascades/haarcascade_eye.xml')
lbp_face_cascade = cv.CascadeClassifier(f'{haar_path}/lbpcascades/lbpcascade_frontalface.xml')

faces = face_cascade.detectMultiScale(gray, 1.3, 5)
lbp_faces = lbp_face_cascade.detectMultiScale(gray, 1.3, 5)
print(faces)

for (x, y, w, h) in faces:
    cv.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
    roi_gray = gray[y:y + h, x:x + w]
    roi_color = img[y:y + h, x:x + w]
    eyes = eye_cascade.detectMultiScale(roi_gray)
    for (ex, ey, ew, eh) in eyes:
        cv.rectangle(roi_color, (ex, ey), (ex + ew, ey + eh), (0, 255, 0), 2)

