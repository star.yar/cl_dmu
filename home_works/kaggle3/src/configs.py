import argparse
import torch

from data_utils import count_unique_faces
from utils import get_datetime_now


def load_args():
    """
    Loads passes arguments

    Returns:
        arguments (dict):
    """

    # ad-hoc params creation
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    now = get_datetime_now()

    # default_model_names = sorted(name for name in models.__dict__
    #                              if name.islower() and not name.startswith("__")
    #                              and callable(models.__dict__[name]))
    #
    # customized_models_names = sorted(name for name in customized_models.__dict__
    #                                  if name.islower() and not name.startswith("__")
    #                                  and callable(customized_models.__dict__[name]))
    #
    # for name in customized_models.__dict__:
    #     if name.islower() and not name.startswith("__") and callable(customized_models.__dict__[name]):
    #         models.__dict__[name] = customized_models.__dict__[name]
    #
    # model_names = default_model_names + customized_models_names

    # Parse arguments
    parser = argparse.ArgumentParser(description='DL Framework by @star.yar')
    # parser.add_argument('-d', '--data', default='path to dataset', type=str)
    # parser.add_argument('--arch', '-a', metavar='ARCH', default='resnet18',
    #                     choices=model_names,
    #                     help='model architecture: ' +
    #                          ' | '.join(model_names) +
    #                          ' (default: resnet18)')
    parser.add_argument('-j', '--workers', default=4, type=int, metavar='N',
                        help='number of data loading workers (default: 4)')

    # Optimization options
    parser.add_argument('--epochs', default=1, type=int, metavar='N',
                        help='number of total epochs to run')
    parser.add_argument('--start-epoch', default=0, type=int, metavar='N',
                        help='manual epoch number (useful on restarts)')
    parser.add_argument('--train-batch', default=256, type=int, metavar='N',
                        help='train batchsize (default: 256)')
    parser.add_argument('--test-batch', default=2048, type=int, metavar='N',
                        help='test batchsize (default: 200)')
    parser.add_argument('--lr', '--learning-rate', default=0.0001, type=float,
                        metavar='LR', help='initial learning rate')
    # parser.add_argument('--lr-decay', type=str, default='step',
    #                     help='mode for learning rate decay')
    parser.add_argument('--step', type=int, default=1,
                        help='interval for learning rate decay in step mode')
    # parser.add_argument('--schedule', type=int, nargs='+', default=[150, 225],
    #                     help='decrease learning rate at these epochs.')
    # parser.add_argument('--turning-point', type=int, default=100,
    #                     help='epoch number from linear to exponential decay mode')
    parser.add_argument('--gamma', type=float, default=0.98,
                        help='LR is multiplied by gamma on schedule.')
    parser.add_argument('--momentum', default=0.9, type=float, metavar='M',
                        help='momentum')
    parser.add_argument('--weight-decay', '--wd', default=1e-5, type=float,
                        metavar='W', help='weight decay (default: 1e-5)')

    parser.add_argument('-c', '--checkpoint', default='../checkpoints', type=str, metavar='PATH',
                        help='path to save checkpoint (default: ../checkpoints)')
    parser.add_argument('-cl', '--checkpoint-lp', action='store_true',
                        help='Use saved optim data (default: False)')
    parser.add_argument('-cf', '--checkpoint-freq', default=10, type=int, metavar='N',
                        help='Checkpoint freq (default: 10)')
    parser.add_argument('--resume', default='', type=str, metavar='PATH',
                        help='path to latest checkpoint (default: none)')

    # # Architecture
    # parser.add_argument('--cardinality', type=int, default=32, help='ResNeXt model cardinality (group).')
    # parser.add_argument('--base-width', type=int, default=4, help='ResNeXt model base width.')
    # parser.add_argument('--groups', type=int, default=3, help='ShuffleNet model groups')

    # # Miscs
    # parser.add_argument('--manual-seed', type=int, help='manual seed')
    # parser.add_argument('-e', '--evaluate', dest='evaluate', action='store_true',
    #                     help='evaluate model on validation set')
    # parser.add_argument('--world-size', default=1, type=int,
    #                     help='number of distributed processes')
    # parser.add_argument('--dist-url', default='tcp://224.66.41.62:23456', type=str,
    #                     help='url used to set up distributed training')
    # parser.add_argument('--dist-backend', default='gloo', type=str,
    #                     help='distributed backend')

    # Device options
    parser.add_argument('--device', default=device, type=str,
                        help=f'Device name to train on (default: {device})')
    # Project dependant
    parser.add_argument('--experiment-name', default=now, type=str,
                        help=f'Experiment name (default: {now})')
    parser.add_argument('--task', default='class', type=str,
                        help=f'Experiment task one of [class/valid/feat] (default: class)')
    parser.add_argument('--pretrained-path', default='', type=str, metavar='P_PATH',
                        help='path to pretrained model')
    parser.add_argument('--micro-log', default=0, type=int, metavar='N',
                        help='Log every (n) iterations in epoch (default: 0)')
    parser.add_argument('--input-size', default=32, type=int, metavar='N',
                        help='Model input shape (H, W) to fit pic into (default: 32')
    parser.add_argument('--n-classes', default=0, type=int, metavar='N',
                        help='Num classes, if not passed - will infer ad-hoc')

    # PATHS
    parser.add_argument('--dataset-size', default='medium', type=str, metavar='SIZE',
                        help='Dataset folder to use (default: medium')
    parser.add_argument('--data-path', default='../data', type=str, metavar='PATH',
                        help='Where to store logs (default: ../logs)')
    parser.add_argument('--results-path', default='../results', type=str, metavar='PATH',
                        help='Where to store results (default: ../results)')
    parser.add_argument('--logs-path', default='../logs', type=str, metavar='PATH',
                        help='Where to store logs (default: ../logs)')
    args = vars(parser.parse_args())

    # create sub paths for quick ref in project
    data_path = args["data_path"]
    dataset_size = args['dataset_size']
    args['train_path'] = '{}/train_data/{}'.format(data_path, dataset_size)
    args['val_path'] = '{}/validation_classification/{}'.format(data_path, dataset_size)
    args['test_path'] = '{}/test_classification/{}'.format(data_path, dataset_size)

    n_classes = args['n_classes']
    args['n_classes'] = count_unique_faces(args['train_path']) if n_classes < 1 else n_classes

    return args


# prep env (load out for tracability) ###############
args = load_args()
#####################################################

DEVICE = args['device']

TASK = args['task']

PRETRAINED_PATH = args['pretrained_path']
CP_PATH = args['checkpoint']
RESUME = args['resume']
CP_EVERY = args['checkpoint_freq']
LOAD_OPT = args['checkpoint_lp']

RESULTS_PATH = args['results_path']
LOGS_PATH = args['logs_path']
DATA_PATH = args['data_path']
TRAIN_PATH = args['train_path']
VAL_PATH = args['val_path']
TEST_PATH = args['test_path']

START_EPOCH = args['start_epoch']
EPOCHS = args['epochs']

MICRO_LOG_EVERY = args['micro_log']
EXPERIMENT_NAME = args['experiment_name']

IMG_SIZE = args['input_size']
NUM_CLASSES = args['n_classes']

# TRAIN_BATCH_SIZE = args['train_batch']  # TODO: set use passed value after debug on 1d seq is finished
TRAIN_BATCH_SIZE = 2
TEST_BATCH_SIZE = args['test_batch']
WORKERS = args['workers']

LR = args['lr']
MOMENTUM = args['momentum']
WEIGHT_DECAY = args['weight_decay']
STEP = args['step']
GAMMA = args['gamma']

SHOW_BATCH = False
