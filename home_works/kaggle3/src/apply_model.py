import numpy as np
import pandas as pd
import torch
from torch.utils.data import DataLoader

from data_utils import TestSampler, TestFaceDataset, build_transformers
from utils import get_item
from configs import \
    EXPERIMENT_NAME, DEVICE, \
    TEST_BATCH_SIZE, \
    TEST_PATH, RESULTS_PATH, DATA_PATH, \
    IMG_SIZE, \
    WORKERS

# prepare data
dataset = TestFaceDataset(TEST_PATH, transform=build_transformers('test', IMG_SIZE))
data_loader = DataLoader(dataset, sampler=TestSampler('{}/test_order_classification.txt'.format(DATA_PATH)),
                         batch_size=TEST_BATCH_SIZE, shuffle=False, drop_last=False, num_workers=WORKERS)

# load model
experiment = torch.load('{}/experiment_{}.pt'.format(RESULTS_PATH, EXPERIMENT_NAME))
model = experiment['model']
model.to(DEVICE)
model.eval()

prediction = None
for inputs in data_loader:
    inputs = inputs.to(DEVICE)
    with torch.set_grad_enabled(False):
        outputs = model(inputs)
        _, preds = torch.max(outputs, 1)
        preds = get_item(preds)
        prediction = np.append(prediction, preds) if prediction is not None else preds

df = pd.DataFrame(prediction, index=np.arange(0, len(prediction))).reset_index()
df.columns = ['id', 'label']

save_path = '{}/submission_{}.csv'.format(RESULTS_PATH, EXPERIMENT_NAME)
df.to_csv(save_path, index=False)
print(f"Saved at {save_path}")
