import sys
import torch
from pprint import pprint

# load path
cli_params = sys.argv
assert len(cli_params) > 1
experiment_path = cli_params[1]

# load experiment
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
checkpoint_data = torch.load(experiment_path, map_location=device)

print('Saved data:', list(checkpoint_data.keys()))
print('-'*10)
print()

print(f'Last epoch: {checkpoint_data["epoch"]}')
print('-'*10)
print()

print('Experiment config: ')
print('-'*10)
pprint(checkpoint_data['params'])
print()

print('Opt state:')
print('-'*10)
opt_state = checkpoint_data['optimizer']['param_groups'][0]
opt_state.pop('params')
pprint(opt_state)
