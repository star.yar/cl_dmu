import numpy as np
import math


class Linear():
    # DO NOT DELETE
    def __init__(self, in_feature, out_feature):
        self.in_feature = in_feature
        self.out_feature = out_feature

        self.W = np.random.randn(out_feature, in_feature)
        self.b = np.zeros(out_feature)

        self.dW = np.zeros(self.W.shape)
        self.db = np.zeros(self.b.shape)

    def __call__(self, x):
        return self.forward(x)

    def forward(self, x):
        self.x = x
        self.out = x.dot(self.W.T) + self.b
        return self.out

    def backward(self, delta):
        self.db = delta
        self.dW = np.dot(self.x.T, delta)
        dx = np.dot(delta, self.W.T)
        return dx


class Conv1D():
    def __init__(self, in_channel, out_channel,
                 kernel_size, stride):
        self.in_channel = in_channel
        self.out_channel = out_channel
        self.kernel_size = kernel_size
        self.stride = stride

        self.W = np.random.randn(out_channel, in_channel, kernel_size)
        self.b = np.zeros(out_channel)

        self.dW = np.zeros(self.W.shape)
        self.db = np.zeros(self.b.shape)

        self.x = None
        self.z = None
        self.new_size = None

    def __call__(self, x):
        return self.forward(x)

    def forward(self, x):
        self.batch, __, self.width = x.shape
        assert __ == self.in_channel, 'Expected the inputs to have {} channels'.format(self.in_channel)

        idx_generator = range(0, self.width - self.kernel_size + 1, self.stride)
        self.new_size = len(idx_generator)
        self.z = np.zeros((self.batch, self.out_channel, self.new_size))
        self.x = x

        for s_idx, i in enumerate(idx_generator):
            i_1 = i + self.kernel_size
            self.z[..., s_idx] = (np.tensordot(x[..., i:i_1],
                                               self.W,
                                               axes=([1, 2], [1, 2]))
                                  + self.b)

        return self.z

    def backward(self, delta):
        dx = np.zeros(self.x.shape)

        for s_idx in range(self.new_size):
            i_0 = s_idx * self.stride
            i_1 = i_0 + self.kernel_size

            dx[..., i_0:i_1] += np.tensordot(delta[..., s_idx],
                                             self.W,
                                             axes=([1], [0]))

            self.dW += np.tensordot(delta[..., s_idx],
                                    self.x[..., i_0:i_1],
                                    axes=([0], [0]))
            self.db += delta[..., s_idx].sum(0)

        return dx


class Flatten():
    def __init__(self):
        self.batch_size = None
        self.in_channel = None
        self.in_width = None

    def __call__(self, x):
        return self.forward(x)

    def forward(self, x):
        self.batch_size, self.in_channel, self.in_width = x.shape
        return x.reshape(self.batch_size, self.in_channel * self.in_width)

    def backward(self, x):
        return x.reshape(self.batch_size, self.in_channel, self.in_width)


class ReLU():
    def __call__(self, x):
        return self.forward(x)

    def forward(self, x):
        self.dy = (x >= 0).astype(x.dtype)
        return x * self.dy

    def backward(self, delta):
        return self.dy * delta
