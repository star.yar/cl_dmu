from layers import *


class CNN_B():
    def __init__(self):
        self.layers = [
            Conv1D(24, 8, 8, 4),
            ReLU(),
            Conv1D(8, 16, 1, 1),
            ReLU(),
            Conv1D(16, 4, 1, 1),
            Flatten()
        ]

    def __call__(self, x):
        return self.forward(x)

    def init_weights(self, weights):
        # Load the weights for your CNN from the MLP Weights given
        for layer, w in zip([self.layers[0],
                             self.layers[2],
                             self.layers[4]],
                            weights):

            for out_layer_idx in range(layer.out_channel):
                w_ = w.T[out_layer_idx]
                w_ = w_.reshape(layer.kernel_size, layer.in_channel).T
                layer.W[out_layer_idx] = w_

    def forward(self, x):
        # You do not need to modify this method
        out = x
        for layer in self.layers:
            out = layer(out)
        return out

    def backward(self, delta):
        # You do not need to modify this method
        for layer in self.layers[::-1]:
            delta = layer.backward(delta)
        return delta


class CNN_C():
    def __init__(self):
        # Your initialization code goes here
        self.layers = [
            Conv1D(24, 2, 2, 2),
            ReLU(),
            Conv1D(2, 8, 2, 2),
            ReLU(),
            Conv1D(8, 4, 2, 1),
            Flatten()
        ]

    def __call__(self, x):
        return self.forward(x)

    def init_weights(self, weights):
        i = 0
        # Load the weights for your CNN from the MLP Weights given
        for layer, w in zip([self.layers[0],
                             self.layers[2],
                             self.layers[4]],
                            weights):
            i += 1
            w_active = w[:layer.in_channel * layer.kernel_size, :layer.out_channel]
            print()
            for out_layer_idx in range(layer.out_channel):
                w_ = w_active[:, out_layer_idx]
                w_ = w_.reshape(layer.kernel_size, layer.in_channel).T
                layer.W[out_layer_idx] = w_

    def forward(self, x):
        # You do not need to modify this method
        out = x
        for layer in self.layers:
            out = layer(out)
        return out

    def backward(self, delta):
        # You do not need to modify this method
        for layer in self.layers[::-1]:
            delta = layer.backward(delta)
        return delta
