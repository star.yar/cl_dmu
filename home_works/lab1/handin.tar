hw1/                                                                                                000755  000765  000024  00000000000 13435516106 012550  5                                                                                                    ustar 00star_yar                        staff                           000000  000000                                                                                                                                                                         hw1/__pycache__/                                                                                    000755  000765  000024  00000000000 13432207406 014755  5                                                                                                    ustar 00star_yar                        staff                           000000  000000                                                                                                                                                                         hw1/hw1.py                                                                                          000644  000765  000024  00000026074 13435516050 013630  0                                                                                                    ustar 00star_yar                        staff                           000000  000000                                                                                                                                                                         """
Follow the instructions provided in the writeup to completely
implement the class specifications for a basic MLP, optimizer, .
You will be able to test each section individually by submitting
to autolab after implementing what is required for that section
-- do not worry if some methods required are not implemented yet.

Notes:

The __call__ method is a special reserved method in
python that defines the behaviour of an object when it is
used as a function. For example, take the Linear activation
function whose implementation has been provided.

# >>> activation = Identity()
# >>> activation(3)
# 3
# >>> activation.forward(3)
# 3
"""

# Do not import any additional 3rd party external libraries as they will not
# be available to AutoLab and are not needed (or allowed)
import numpy as np
import os


class Activation(object):
    """
    Interface for activation functions (non-linearities).

    In all implementations, the state attribute must contain the result, i.e. the output of forward (it will be tested).
    """

    # No additional work is needed for this class, as it acts like an abstract base class for the others

    def __init__(self):
        self.state = None

    def __call__(self, x):
        return self.forward(x)

    def forward(self, x):
        raise NotImplemented

    def derivative(self):
        raise NotImplemented


class Identity(Activation):
    """
    Identity function (already implemented).
    """

    # This class is a gimme as it is already implemented for you as an example

    def __init__(self):
        super(Identity, self).__init__()

    def forward(self, x):
        self.state = x
        return x

    def derivative(self):
        return 1.0


class Sigmoid(Activation):
    """
    Sigmoid non-linearity
    """

    def __init__(self):
        super(Sigmoid, self).__init__()

    def forward(self, x):
        self.state = 1 / (1 + np.exp(-x))
        return self.state

    def derivative(self):
        f = self.state
        return f * (1 - f)


class Tanh(Activation):
    """
    Tanh non-linearity
    """

    def __init__(self):
        super(Tanh, self).__init__()

    def forward(self, x):
        self.state = np.tanh(x)
        return self.state

    def derivative(self):
        f = self.state
        return 1 - np.power(f, 2)


class ReLU(Activation):
    """
    ReLU non-linearity
    """

    def __init__(self):
        super(ReLU, self).__init__()
        self.x = None

    def forward(self, x):
        self.x = x
        self.state = np.clip(x, 0, x.max())
        return self.state

    def derivative(self):
        d_f = np.zeros_like(self.x)
        d_f[np.where(self.x > 0)] = 1
        return d_f


# Ok now things get decidedly more interesting. The following Criterion class
# will be used again as the basis for a number of loss functions (which are in the
# form of classes so that they can be exchanged easily (it's how PyTorch and other
# ML libraries do it))


class Criterion(object):
    """
    Interface for loss functions.
    """

    # Nothing needs done to this class, it's used by the following Criterion classes

    def __init__(self):
        self.logits = None
        self.labels = None
        self.loss = None

    def __call__(self, x, y):
        return self.forward(x, y)

    def forward(self, x, y):
        raise NotImplemented

    def derivative(self):
        raise NotImplemented


class SoftmaxCrossEntropy(Criterion):
    """
    Softmax loss
    """

    # • Input shapes:
    #   – x: (batch size, 10)
    #   – y: (batch size, 10)
    # • Output Shape:
    #   – out: (batch size,)

    def __init__(self):
        super(SoftmaxCrossEntropy, self).__init__()
        self.sm = None

    def forward(self, x, y):
        self.labels = y
        self.logits = x

        x_max = np.max(x)  # if abs(np.max(x)) > abs(np.min(x)) else -np.min(x)

        self.sm = (np.exp(x - x_max) / np.exp(x - x_max).sum(axis=1).reshape(-1, 1))
        self.loss = (-1) * (y * np.log(self.sm)).sum(axis=1)
        return self.loss

    def derivative(self):
        return self.sm - self.labels


class BatchNorm(object):

    def __init__(self, fan_in, alpha=0.9):
        # You shouldn't need to edit anything in init

        self.alpha = alpha
        self.eps = 1e-8
        self.x = None
        self.norm = None
        self.out = None

        # The following attributes will be tested
        self.var = np.ones((1, fan_in))
        self.mean = np.zeros((1, fan_in))

        self.gamma = np.ones((1, fan_in))
        self.dgamma = np.zeros((1, fan_in))

        self.beta = np.zeros((1, fan_in))
        self.dbeta = np.zeros((1, fan_in))

        # inference parameters
        self.running_mean = np.zeros((1, fan_in))
        self.running_var = np.ones((1, fan_in))

    def __call__(self, x, eval=False):
        return self.forward(x, eval)

    def forward(self, x, eval=False):
        if eval:
            norm = (x - self.running_mean) / (self.running_var + self.eps) ** 0.5
            return norm * self.gamma + self.beta

        self.x = x

        self.mean = x.mean(axis=0)
        self.var = x.var(axis=0)

        self.norm = (x - self.mean)/(self.var+self.eps)**(1/2)
        self.out = self.norm * self.gamma + self.beta

        # update running batch statistics
        self.running_mean = self.alpha * self.running_mean + (1 - self.alpha) * self.mean
        self.running_var = self.alpha * self.running_var + (1 - self.alpha) * self.var

        return self.out

    def backward(self, delta):

        batch_size, n_features = self.x.shape

        dL_norm = delta * self.gamma
        x_mean = self.x - self.mean

        # Sigma
        std_inv = 1. / np.sqrt(self.var + self.eps)
        dL_var = -.5 * (dL_norm * x_mean).sum(axis=0) * std_inv ** 3

        # Mu
        dL_mean = - (dL_norm * std_inv).sum(axis=0) \
                  - 2. * dL_var * x_mean.mean(axis=0)

        self.dgamma = (delta * self.norm).sum(axis=0) #* self.batch_size
        self.dbeta = delta.sum(axis=0)#* self.batch_size

        dL_x = (dL_norm * std_inv) \
               + (2 / batch_size) * dL_var * x_mean \
               + dL_mean / batch_size

        return dL_x


def random_normal_weight_init(d0, d1):
    return np.random.normal(size=(d0, d1))


def zeros_bias_init(d):
    return np.zeros(d)


class MLP(object):
    """
    A simple multilayer perceptron
    """

    def __init__(self, input_size, output_size, hiddens, activations, weight_init_fn, bias_init_fn, criterion, lr,
                 momentum=0.0, num_bn_layers=0):
        # Don't change this -->
        self.train_mode = True
        self.num_bn_layers = num_bn_layers
        self.bn = num_bn_layers > 0
        self.nlayers = len(hiddens) + 1
        self.input_size = input_size
        self.output_size = output_size
        self.activations = activations
        self.criterion = criterion
        self.lr = lr
        self.momentum = momentum
        # <---------------------

        self.in_sizes = [input_size] + hiddens[:self.nlayers - 1]
        self.out_sizes = hiddens + [output_size]

        self.W = [weight_init_fn(self.in_sizes[i], self.out_sizes[i]) for i in range(self.nlayers)]
        self.b = [bias_init_fn(self.out_sizes[i]) for i in range(self.nlayers)]

        self.dW = [np.zeros_like(self.W[i]) for i in range(self.nlayers)]
        self.db = [np.zeros(self.out_sizes[i]) for i in range(self.nlayers)]

        if momentum:
            self.running_dW = None
            self.running_db = None

        self.y = [None] * (self.nlayers + 1)
        self.err = None

        # if batch norm, add batch norm parameters
        if self.bn:
            self.bn_layers = [BatchNorm(self.out_sizes[i]) for i in range(self.num_bn_layers)]

    def forward(self, x):
        self.y[0] = x

        for i in range(1, self.nlayers+1):
            z = self.y[i-1] @ self.W[i-1] + self.b[i-1]
            if self.num_bn_layers>=i:
                z = self.bn_layers[i-1].forward(z, not self.train_mode)
            self.y[i] = self.activations[i-1](z)

        return self.y[-1]

    def zero_grads(self):
        if self.momentum:
            self.running_dW = self.dW
            self.running_db = self.db

        self.dW = [np.zeros_like(self.W[i]) for i in range(self.nlayers)]
        self.db = [np.zeros(self.out_sizes[i]) for i in range(self.nlayers)]

    def step(self):
        for i in range(0, self.nlayers):
            deltaW = self.dW[i]
            deltab = self.db[i]

            if self.momentum:
                deltaW = deltaW if self.running_dW is None else self.momentum * self.running_dW[i] + (
                            1 - self.momentum) * deltaW
                deltab = deltab if self.running_db is None else self.momentum * self.running_db[i] + (
                            1 - self.momentum) * deltab

            self.W[i] -= self.lr * deltaW
            self.b[i] -= self.lr * deltab


        if self.bn:
            for bn_layer in self.bn_layers:
                bn_layer.beta -= self.lr * bn_layer.dbeta
                bn_layer.gamma-= self.lr * bn_layer.dgamma


    def backward(self, labels):
        self.criterion(self.y[self.nlayers], labels)

        dDiv_z = [None] * (self.nlayers + 1)
        dDiv_y = [None] * (self.nlayers + 1)

        batch_size = self.criterion.loss.shape[0]
        dDiv_y[self.nlayers] = self.criterion.derivative() / batch_size

        for i in reversed(range(0, self.nlayers)):
            dDiv_z[i] = dDiv_y[i+1] * self.activations[i].derivative()

            if i < self.num_bn_layers:
                dDiv_z_norm = self.bn_layers[i].backward(dDiv_z[i] * batch_size)
                dDiv_z[i] = dDiv_z_norm / batch_size

            dDiv_y[i] = dDiv_z[i] @ self.W[i].T


            self.dW[i] += self.y[i].T @ dDiv_z[i]
            self.db[i] += dDiv_z[i].sum(0)

    def __call__(self, x):
        return self.forward(x)

    def train(self):
        self.train_mode = True

    def eval(self):
        self.train_mode = False


def get_training_stats(mlp, dset, nepochs, batch_size):
    train, val, test = dset
    trainx, trainy = train
    valx, valy = val
    testx, testy = test

    idxs = np.arange(len(trainx))

    training_losses = []
    training_errors = []
    validation_losses = []
    validation_errors = []

    # Setup ...

    for e in range(nepochs):

        # Per epoch setup ...

        for b in range(0, len(trainx), batch_size):
            pass  # Remove this line when you start implementing this
            # Train ...

        for b in range(0, len(valx), batch_size):
            pass  # Remove this line when you start implementing this
            # Val ...

        # Accumulate data...

    # Cleanup ...

    for b in range(0, len(testx), batch_size):
        pass  # Remove this line when you start implementing this
        # Test ...

    # Return results ...

    # return (training_losses, training_errors, validation_losses, validation_errors)

    raise NotImplemented

# import matplotlib.pyplot as plt
#
# x = np.random.normal(0, 1, size=(100))
# y = 10+5*x>10
#
# x_test = np.random.normal(0, 1, size=(100))
# y_test = 10+5*x>10
#
# plt.scatter(x, y, c='blue')
# plt.scatter(x_test, y_test, c='red')
# plt.show()
#
# mlp = MLP(x.shape)
# print()
                                                                                                                                                                                                                                                                                                                                                                                                                                                                    hw1/__pycache__/hw1.cpython-36.pyc                                                                  000644  000765  000024  00000021435 13432207374 020113  0                                                                                                    ustar 00star_yar                        staff                           000000  000000                                                                                                                                                                         3
�i\�!  �               @   s�   d Z ddlZddlZddlZdZG dd� de�ZG dd� de�ZG dd	� d	e�Z	G d
d� de�Z
G dd� de�ZG dd� de�ZG dd� de�ZG dd� de�Zdd� Zdd� ZG dd� de�Zdd� ZdS )a~  
Follow the instructions provided in the writeup to completely
implement the class specifications for a basic MLP, optimizer, .
You will be able to test each section individually by submitting
to autolab after implementing what is required for that section
-- do not worry if some methods required are not implemented yet.

Notes:

The __call__ method is a special reserved method in
python that defines the behaviour of an object when it is
used as a function. For example, take the Linear activation
function whose implementation has been provided.

# >>> activation = Identity()
# >>> activation(3)
# 3
# >>> activation.forward(3)
# 3
�    Ng:�0�yE>c               @   s0   e Zd ZdZdd� Zdd� Zdd� Zdd	� Zd
S )�
Activationz�
    Interface for activation functions (non-linearities).

    In all implementations, the state attribute must contain the result, i.e. the output of forward (it will be tested).
    c             C   s
   d | _ d S )N)�state)�self� r   �A   /Users/star_yar/Desktop/DL🎓Fall2018/home_works/lab1/hw1/hw1.py�__init__(   s    zActivation.__init__c             C   s
   | j |�S )N)�forward)r   �xr   r   r   �__call__+   s    zActivation.__call__c             C   s   t �d S )N)�NotImplemented)r   r	   r   r   r   r   .   s    zActivation.forwardc             C   s   t �d S )N)r   )r   r   r   r   �
derivative1   s    zActivation.derivativeN)�__name__�
__module__�__qualname__�__doc__r   r
   r   r   r   r   r   r   r      s
   r   c                   s0   e Zd ZdZ� fdd�Zdd� Zdd� Z�  ZS )�Identityz2
    Identity function (already implemented).
    c                s   t t| �j�  d S )N)�superr   r   )r   )�	__class__r   r   r   <   s    zIdentity.__init__c             C   s
   || _ |S )N)r   )r   r	   r   r   r   r   ?   s    zIdentity.forwardc             C   s   dS )Ng      �?r   )r   r   r   r   r   C   s    zIdentity.derivative)r   r   r   r   r   r   r   �__classcell__r   r   )r   r   r   5   s   r   c                   s0   e Zd ZdZ� fdd�Zdd� Zdd� Z�  ZS )�Sigmoidz
    Sigmoid non-linearity
    c                s   t t| �j�  d S )N)r   r   r   )r   )r   r   r   r   L   s    zSigmoid.__init__c             C   s   ddt j| �  | _| jS )N�   )�np�expr   )r   r	   r   r   r   r   O   s    zSigmoid.forwardc             C   s   | j }|d|  S )Nr   )r   )r   �fr   r   r   r   S   s    zSigmoid.derivative)r   r   r   r   r   r   r   r   r   r   )r   r   r   G   s   r   c                   s0   e Zd ZdZ� fdd�Zdd� Zdd� Z�  ZS )�Tanhz
    Tanh non-linearity
    c                s   t t| �j�  d S )N)r   r   r   )r   )r   r   r   r   ]   s    zTanh.__init__c             C   s   t j|�| _| jS )N)r   �tanhr   )r   r	   r   r   r   r   `   s    zTanh.forwardc             C   s   | j }dtj|d� S )Nr   �   )r   r   �power)r   r   r   r   r   r   d   s    zTanh.derivative)r   r   r   r   r   r   r   r   r   r   )r   r   r   X   s   r   c                   s0   e Zd ZdZ� fdd�Zdd� Zdd� Z�  ZS )�ReLUz
    ReLU non-linearity
    c                s   t t| �j�  d | _d S )N)r   r   r   r	   )r   )r   r   r   r   n   s    zReLU.__init__c             C   s    || _ tj|d|j� �| _| jS )Nr   )r	   r   �clip�maxr   )r   r	   r   r   r   r   r   s    zReLU.forwardc             C   s(   t j| j�}d|t j| jdt k�< |S )Nr   r   )r   �
zeros_liker	   �where�EPS)r   Zd_fr   r   r   r   w   s    zReLU.derivative)r   r   r   r   r   r   r   r   r   r   )r   r   r   i   s   r   c               @   s0   e Zd ZdZdd� Zdd� Zdd� Zdd	� Zd
S )�	Criterionz'
    Interface for loss functions.
    c             C   s   d | _ d | _d | _d S )N)�logits�labelsZloss)r   r   r   r   r   �   s    zCriterion.__init__c             C   s   | j ||�S )N)r   )r   r	   �yr   r   r   r
   �   s    zCriterion.__call__c             C   s   t �d S )N)r   )r   r	   r'   r   r   r   r   �   s    zCriterion.forwardc             C   s   t �d S )N)r   )r   r   r   r   r   �   s    zCriterion.derivativeN)r   r   r   r   r   r
   r   r   r   r   r   r   r$   �   s
   r$   c                   s0   e Zd ZdZ� fdd�Zdd� Zdd� Z�  ZS )�SoftmaxCrossEntropyz
    Softmax loss
    c                s   t t| �j�  d | _d S )N)r   r(   r   �sm)r   )r   r   r   r   �   s    zSoftmaxCrossEntropy.__init__c             C   sT   || _ || _tj|�tj|�jdd�j|jd d� | _d|tj| j� jdd� S )Nr   )�axisr   �����)	r%   r&   r   r   �sum�reshape�shaper)   �log)r   r	   r'   r   r   r   r   �   s    ,zSoftmaxCrossEntropy.forwardc             C   s   d| j  | j S )Nr   r+   )r&   r)   )r   r   r   r   r   �   s    zSoftmaxCrossEntropy.derivative)r   r   r   r   r   r   r   r   r   r   )r   r   r(   �   s   r(   c               @   s2   e Zd Zddd�Zddd�Zddd�Zd	d
� ZdS )�	BatchNorm��������?c             C   s�   || _ d| _d | _d | _d | _tjd|f�| _tjd|f�| _	tjd|f�| _
tjd|f�| _tjd|f�| _tjd|f�| _tjd|f�| _tjd|f�| _d S )Ng:�0�yE>r   )�alpha�epsr	   �norm�outr   �ones�var�zeros�mean�gamma�dgamma�beta�dbetaZrunning_meanZrunning_var)r   Zfan_inr2   r   r   r   r   �   s    zBatchNorm.__init__Fc             C   s   | j ||�S )N)r   )r   r	   �evalr   r   r   r
   �   s    zBatchNorm.__call__c             C   s   || _ t�d S )N)r	   r   )r   r	   r>   r   r   r   r   �   s    zBatchNorm.forwardc             C   s   t �d S )N)r   )r   �deltar   r   r   �backward�   s    zBatchNorm.backwardN)r1   )F)F)r   r   r   r   r
   r   r@   r   r   r   r   r0   �   s   


r0   c             C   s   t �d S )N)r   )�d0�d1r   r   r   �random_normal_weight_init�   s    rC   c             C   s   t �d S )N)r   )�dr   r   r   �zeros_bias_init�   s    rE   c               @   sR   e Zd ZdZddd�Zdd� Zdd	� Zd
d� Zdd� Zdd� Z	dd� Z
dd� ZdS )�MLPz(
    A simple multilayer perceptron
    �        r   c             C   sp   d| _ |
| _|
dk| _t|�d | _|| _|| _|| _|| _|| _	|	| _
d | _d | _d | _d | _| jrld | _d S )NTr   r   )�
train_mode�num_bn_layers�bn�lenZnlayers�
input_size�output_size�activations�	criterion�lr�momentum�W�dW�b�db�	bn_layers)r   rL   rM   ZhiddensrN   Zweight_init_fnZbias_init_fnrO   rP   rQ   rI   r   r   r   r     s     
zMLP.__init__c             C   s   t �d S )N)r   )r   r	   r   r   r   r   %  s    zMLP.forwardc             C   s   t �d S )N)r   )r   r   r   r   �
zero_grads(  s    zMLP.zero_gradsc             C   s   t �d S )N)r   )r   r   r   r   �step+  s    zMLP.stepc             C   s   t �d S )N)r   )r   r&   r   r   r   r@   .  s    zMLP.backwardc             C   s
   | j |�S )N)r   )r   r	   r   r   r   r
   1  s    zMLP.__call__c             C   s
   d| _ d S )NT)rH   )r   r   r   r   �train4  s    z	MLP.trainc             C   s
   d| _ d S )NF)rH   )r   r   r   r   r>   7  s    zMLP.evalN)rG   r   )r   r   r   r   r   r   rW   rX   r@   r
   rY   r>   r   r   r   r   rF     s   
rF   c             C   s�   |\}}}|\}}|\}	}
|\}}t jt|��}g }g }g }g }xDt|�D ]8}xtdt|�|�D ]}q`W xtdt|	�|�D ]}qzW qJW xtdt|�|�D ]}q�W t�d S )Nr   )r   �arangerK   �ranger   )�mlpZdsetZnepochs�
batch_sizerY   �val�testZtrainxZtrainyZvalxZvalyZtestxZtesty�idxsZtraining_lossesZtraining_errorsZvalidation_lossesZvalidation_errors�erT   r   r   r   �get_training_stats;  s"    
rb   )r   �numpyr   �osZtorchr#   �objectr   r   r   r   r   r$   r(   r0   rC   rE   rF   rb   r   r   r   r   �<module>   s    -49                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   