"""
Follow the instructions provided in the writeup to completely
implement the class specifications for a basic MLP, optimizer, .
You will be able to test each section individually by submitting
to autolab after implementing what is required for that section
-- do not worry if some methods required are not implemented yet.

Notes:

The __call__ method is a special reserved method in
python that defines the behaviour of an object when it is
used as a function. For example, take the Linear activation
function whose implementation has been provided.

# >>> activation = Identity()
# >>> activation(3)
# 3
# >>> activation.forward(3)
# 3
"""

# Do not import any additional 3rd party external libraries as they will not
# be available to AutoLab and are not needed (or allowed)
import numpy as np
import os


class Activation(object):
    """
    Interface for activation functions (non-linearities).

    In all implementations, the state attribute must contain the result, i.e. the output of forward (it will be tested).
    """

    # No additional work is needed for this class, as it acts like an abstract base class for the others

    def __init__(self):
        self.state = None

    def __call__(self, x):
        return self.forward(x)

    def forward(self, x):
        raise NotImplemented

    def derivative(self):
        raise NotImplemented


class Identity(Activation):
    """
    Identity function (already implemented).
    """

    # This class is a gimme as it is already implemented for you as an example

    def __init__(self):
        super(Identity, self).__init__()

    def forward(self, x):
        self.state = x
        return x

    def derivative(self):
        return 1.0


class Sigmoid(Activation):
    """
    Sigmoid non-linearity
    """

    def __init__(self):
        super(Sigmoid, self).__init__()

    def forward(self, x):
        self.state = 1 / (1 + np.exp(-x))
        return self.state

    def derivative(self):
        f = self.state
        return f * (1 - f)


class Tanh(Activation):
    """
    Tanh non-linearity
    """

    def __init__(self):
        super(Tanh, self).__init__()

    def forward(self, x):
        self.state = np.tanh(x)
        return self.state

    def derivative(self):
        f = self.state
        return 1 - np.power(f, 2)


class ReLU(Activation):
    """
    ReLU non-linearity
    """

    def __init__(self):
        super(ReLU, self).__init__()
        self.x = None

    def forward(self, x):
        self.x = x
        self.state = np.clip(x, 0, x.max())
        return self.state

    def derivative(self):
        d_f = np.zeros_like(self.x)
        d_f[np.where(self.x > 0)] = 1
        return d_f


# Ok now things get decidedly more interesting. The following Criterion class
# will be used again as the basis for a number of loss functions (which are in the
# form of classes so that they can be exchanged easily (it's how PyTorch and other
# ML libraries do it))


class Criterion(object):
    """
    Interface for loss functions.
    """

    # Nothing needs done to this class, it's used by the following Criterion classes

    def __init__(self):
        self.logits = None
        self.labels = None
        self.loss = None

    def __call__(self, x, y):
        return self.forward(x, y)

    def forward(self, x, y):
        raise NotImplemented

    def derivative(self):
        raise NotImplemented


class SoftmaxCrossEntropy(Criterion):
    """
    Softmax loss
    """

    # • Input shapes:
    #   – x: (batch size, 10)
    #   – y: (batch size, 10)
    # • Output Shape:
    #   – out: (batch size,)

    def __init__(self):
        super(SoftmaxCrossEntropy, self).__init__()
        self.sm = None

    def forward(self, x, y):
        self.labels = y
        self.logits = x

        x_max = np.max(x)  # if abs(np.max(x)) > abs(np.min(x)) else -np.min(x)

        self.sm = (np.exp(x - x_max) / np.exp(x - x_max).sum(axis=1).reshape(-1, 1))
        self.loss = (-1) * (y * np.log(self.sm)).sum(axis=1)
        return self.loss

    def derivative(self):
        return self.sm - self.labels


class BatchNorm(object):

    def __init__(self, fan_in, alpha=0.9):
        # You shouldn't need to edit anything in init

        self.alpha = alpha
        self.eps = 1e-8
        self.x = None
        self.norm = None
        self.out = None

        # The following attributes will be tested
        self.var = np.ones((1, fan_in))
        self.mean = np.zeros((1, fan_in))

        self.gamma = np.ones((1, fan_in))
        self.dgamma = np.zeros((1, fan_in))

        self.beta = np.zeros((1, fan_in))
        self.dbeta = np.zeros((1, fan_in))

        # inference parameters
        self.running_mean = np.zeros((1, fan_in))
        self.running_var = np.ones((1, fan_in))

    def __call__(self, x, eval=False):
        return self.forward(x, eval)

    def forward(self, x, eval=False):
        if eval:
            norm = (x - self.running_mean) / (self.running_var + self.eps) ** 0.5
            return norm * self.gamma + self.beta

        self.x = x

        self.mean = x.mean(axis=0)
        self.var = x.var(axis=0)

        self.norm = (x - self.mean)/(self.var+self.eps)**(1/2)
        self.out = self.norm * self.gamma + self.beta

        # update running batch statistics
        self.running_mean = self.alpha * self.running_mean + (1 - self.alpha) * self.mean
        self.running_var = self.alpha * self.running_var + (1 - self.alpha) * self.var

        return self.out

    def backward(self, delta):

        batch_size, n_features = self.x.shape

        dL_norm = delta * self.gamma
        x_mean = self.x - self.mean

        # Sigma
        std_inv = 1. / np.sqrt(self.var + self.eps)
        dL_var = -.5 * (dL_norm * x_mean).sum(axis=0) * std_inv ** 3

        # Mu
        dL_mean = - (dL_norm * std_inv).sum(axis=0) \
                  - 2. * dL_var * x_mean.mean(axis=0)

        self.dgamma = (delta * self.norm).sum(axis=0) #* self.batch_size
        self.dbeta = delta.sum(axis=0)#* self.batch_size

        dL_x = (dL_norm * std_inv) \
               + (2 / batch_size) * dL_var * x_mean \
               + dL_mean / batch_size

        return dL_x


def random_normal_weight_init(d0, d1):
    return np.random.normal(size=(d0, d1))


def zeros_bias_init(d):
    return np.zeros(d)


class MLP(object):
    """
    A simple multilayer perceptron
    """

    def __init__(self, input_size, output_size, hiddens, activations, weight_init_fn, bias_init_fn, criterion, lr,
                 momentum=0.0, num_bn_layers=0):
        # Don't change this -->
        self.train_mode = True
        self.num_bn_layers = num_bn_layers
        self.bn = num_bn_layers > 0
        self.nlayers = len(hiddens) + 1
        self.input_size = input_size
        self.output_size = output_size
        self.activations = activations
        self.criterion = criterion
        self.lr = lr
        self.momentum = momentum
        # <---------------------

        self.in_sizes = [input_size] + hiddens[:self.nlayers - 1]
        self.out_sizes = hiddens + [output_size]

        self.W = [weight_init_fn(self.in_sizes[i], self.out_sizes[i]) for i in range(self.nlayers)]
        self.b = [bias_init_fn(self.out_sizes[i]) for i in range(self.nlayers)]

        self.dW = [np.zeros_like(self.W[i]) for i in range(self.nlayers)]
        self.db = [np.zeros(self.out_sizes[i]) for i in range(self.nlayers)]

        # self.running_dW = [None] * self.nlayers
        # self.running_db = [None] * self.nlayers
        self.running_dW = np.zeros_like(self.dW)
        self.running_db = np.zeros_like(self.db)

        self.y = [None] * (self.nlayers + 1)
        self.err = None

        # if batch norm, add batch norm parameters
        if self.bn:
            self.bn_layers = [BatchNorm(self.out_sizes[i]) for i in range(self.num_bn_layers)]

    def forward(self, x):
        self.y[0] = x

        for i in range(1, self.nlayers+1):
            z = self.y[i-1] @ self.W[i-1] + self.b[i-1]
            if self.num_bn_layers>=i:
                z = self.bn_layers[i-1].forward(z, not self.train_mode)
            self.y[i] = self.activations[i-1](z)

        return self.y[-1]

    def zero_grads(self):
        self.dW = [np.zeros_like(self.W[i]) for i in range(self.nlayers)]
        self.db = [np.zeros(self.out_sizes[i]) for i in range(self.nlayers)]

    def step(self):
        for i in range(0, self.nlayers):
            # # init on first run
            # if self.running_dW[i] is None:
            #     self.running_dW[i], self.running_db[i] = self.dW[i], self.db[i]

            deltaW = self.momentum * self.running_dW[i] + (1 - self.momentum) * self.dW[i]
            deltab = self.momentum * self.running_db[i] + (1 - self.momentum) * self.db[i]

            # save running stats
            self.running_dW[i] = deltaW
            self.running_db[i] = deltab

            # update params
            self.W[i] -= self.lr * deltaW
            self.b[i] -= self.lr * deltab

        if self.bn:
            for bn_layer in self.bn_layers:
                bn_layer.beta -= self.lr * bn_layer.dbeta
                bn_layer.gamma-= self.lr * bn_layer.dgamma


    def backward(self, labels):
        self.criterion(self.y[self.nlayers], labels)

        dDiv_z = [None] * (self.nlayers + 1)
        dDiv_y = [None] * (self.nlayers + 1)

        batch_size = self.criterion.loss.shape[0]
        dDiv_y[self.nlayers] = self.criterion.derivative() / batch_size

        for i in reversed(range(0, self.nlayers)):
            dDiv_z[i] = dDiv_y[i+1] * self.activations[i].derivative()

            if i < self.num_bn_layers:
                dDiv_z_norm = self.bn_layers[i].backward(dDiv_z[i] * batch_size)
                dDiv_z[i] = dDiv_z_norm / batch_size

            dDiv_y[i] = dDiv_z[i] @ self.W[i].T


            self.dW[i] += self.y[i].T @ dDiv_z[i]
            self.db[i] += dDiv_z[i].sum(0)

    def __call__(self, x):
        return self.forward(x)

    def train(self):
        self.train_mode = True

    def eval(self):
        self.train_mode = False


def get_training_stats(mlp, dset, nepochs, batch_size):
    train, val, test = dset
    trainx, trainy = train
    valx, valy = val
    testx, testy = test

    idxs = np.arange(len(trainx))

    training_losses = []
    training_errors = []
    validation_losses = []
    validation_errors = []

    # Setup ...

    for e in range(nepochs):

        # Per epoch setup ...

        for b in range(0, len(trainx), batch_size):
            pass  # Remove this line when you start implementing this
            # Train ...

        for b in range(0, len(valx), batch_size):
            pass  # Remove this line when you start implementing this
            # Val ...

        # Accumulate data...

    # Cleanup ...

    for b in range(0, len(testx), batch_size):
        pass  # Remove this line when you start implementing this
        # Test ...

    # Return results ...

    # return (training_losses, training_errors, validation_losses, validation_errors)

    raise NotImplemented

# import matplotlib.pyplot as plt
#
# x = np.random.normal(0, 1, size=(100))
# y = 10+5*x>10
#
# x_test = np.random.normal(0, 1, size=(100))
# y_test = 10+5*x>10
#
# plt.scatter(x, y, c='blue')
# plt.scatter(x_test, y_test, c='red')
# plt.show()
#
# mlp = MLP(x.shape)
# print()
