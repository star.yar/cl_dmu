import numpy as np
import os
import time
import torch


def _check_array(x, ub=None, lb=None):
    if lb:
        incorrect_subsets = np.array([n.shape[0] for n in x if n.shape[0] < lb])
        if len(incorrect_subsets) > 0:
            raise ValueError("Shape of array is less than requested")

    if ub:
        incorrect_subsets = np.array([n.shape[0] for n in x if n.shape[0] > ub])
        if len(incorrect_subsets) > 0:
            raise ValueError("Shape of array is more than requested")


def sumproducts(x, y):
    """
    x is a 1-dimensional int numpy array.
    y is a 1-dimensional int numpy array.
    Return the sum of x[i] * y[j] for all pairs of indices i, j.

    >>> sumproducts(np.arange(3000), np.arange(3000))
    20_236_502_250_000

    """

    result = 0
    for i in range(len(x)):
        for j in range(len(y)):
            result += x[i] * y[j]
    return result


def vectorize_sumproducts(x, y):
    """
    x is a 1-dimensional int numpy array. Shape of x is (N, ).
    y is a 1-dimensional int numpy array. Shape of y is (N, ).
    Return the sum of x[i] * y[j] for all pairs of indices i, j.

    >>> vectorize_sumproducts(np.arange(3000), np.arange(3000))
    20236502250000

    """

    x = x.reshape(x.shape[0], -1)
    y = y.reshape(-1, y.shape[0])

    return np.matmul(x, y).sum()


def Relu(x):
    """
    x is a 2-dimensional int numpy array.
    Return x[i][j] = 0 if < 0 else x[i][j] for all pairs of indices i, j.

    """
    result = np.copy(x)
    for i in range(x.shape[0]):
        for j in range(x.shape[1]):
            if x[i][j] < 0:
                result[i][j] = 0
    return result


def vectorize_Relu(x):
    """
    x is a 2-dimensional int numpy array.
    Return x[i][j] = 0 if < 0 else x[i][j] for all pairs of indices i, j.

    """

    result = np.clip(np.copy(x), 0, None)
    return result


def ReluPrime(x):
    """
    x is a 2-dimensional int numpy array.
    Return x[i][j] = 0 if x[i][j] < 0 else 1 for all pairs of indices i, j.

    """
    result = np.copy(x)
    for i in range(x.shape[0]):
        for j in range(x.shape[1]):
            if x[i][j] < 0:
                result[i][j] = 0
            else:
                result[i][j] = 1
    return result


def vectorize_PrimeRelu(x):
    """
    x is a 2-dimensional int numpy array.
    Return x[i][j] = 0 if x[i][j] < 0 else 1 for all pairs of indices i, j.

    """

    x = np.copy(x)
    x[np.where(x == 0)] = 1
    result = np.clip(np.copy(x), 0, 1)
    return result


def slice_fixed_point(x, l, start_point):
    """
    x is a 3-dimensional int numpy array, (n, ). First dimension represent the number of instances in the array.
    Second dimension is variable, depending on the length of a given instance. Third dimension is fixed
    to the number of features extracted per utterance in an instance.
    l is an integer representing the length of the utterances that the final array should have.
    start_point is an integer representing the point at which the final utterance should start in.
    Return a 3-dimensional int numpy array of shape (n, l, -1)

    """

    # x = np.copy(x)
    _check_array(x, lb=start_point + l)
    return np.asarray([x_i[start_point: start_point + l] for x_i in x])


def slice_last_point(x, l):
    """
    x is a 3-dimensional int numpy array, (n, ). First dimension represent the number of instances in the array.
    Second dimension is variable, depending on the length of a given instance. Third dimension is fixed
    to the number of features extracted per utterance in an instance.
    l is an integer representing the length of the utterances that the final array should be in.
    Return a 3-dimensional int numpy array of shape (n, l, -1)

    """

    _check_array(x, lb=l)
    return [x_i[x_i.shape[0] - l:] for x_i in x]


def slice_random_point(x, l):
    """
    x is a 3-dimensional int numpy array, (n, ). First dimension represent the number of instances in the array.
    Second dimension is variable, depending on the length of a given instance. Third dimension is fixed
    to the number of features extracted per utterance in an instance.
    l is an integer representing the length of the utterances that the final array should be in.
    Return a 3-dimensional int numpy array of shape (n, l, -1)

    """

    _check_array(x, lb=l)
    start_points = [np.random.randint(0, x_i.shape[0] - l + 1) for x_i in x]

    return np.asarray([x_i[start_points[i]:start_points[i] + l] for i, x_i in enumerate(x)])


def pad_pattern_end(x):
    """
    x is a 3-dimensional int numpy array, (n, ). First dimension represent the number of instances in the array.
    Second dimension is variable, depending on the length of a given instance. Third dimension is fixed
    to the number of features extracted per utterance in an instance.

    Return a 3-dimensional int numpy array.

    """

    x = np.copy(x)
    max_shape = max([inst.shape[0] for inst in x])

    return np.asarray(
        [np.pad(x_i,
                ((0, int(np.ceil(max_shape - x_i.shape[0]))), (0, 0)), 'symmetric')
         for x_i in x])


def pad_constant_central(x, c_):
    """
    x is a 3-dimensional int numpy array, (n, ). First dimension represent the number of instances in the array.
    Second dimension is variable, depending on the length of a given instance. Third dimension is fixed
    to the number of features extracted per utterance in an instance.

    Return a 3-dimensional int numpy array.

    """

    x = np.copy(x)
    max_shape = max([inst.shape[0] for inst in x])

    return np.asarray(
        [np.pad(x_i,
                ((int(np.floor((max_shape - x_i.shape[0]) / 2)),
                  int(np.ceil((max_shape - x_i.shape[0]) / 2))), (0, 0)), 'constant', constant_values=c_)
         for x_i in x])

    # return np.asarray(
    #     [np.pad(x_i,
    #             ((0,
    #               int(np.ceil((max_shape - x_i.shape[0])))), (0, 0)), 'constant', constant_values=-11)
    #      for x_i in x])


def numpy2tensor(x):
    """
    x is an numpy nd-array. 

    Return a pytorch Tensor of the same shape containing the same data.
    """

    return torch.from_numpy(x)


def tensor2numpy(x):
    """
    x is a pytorch Tensor. 

    Return a numpy nd-array of the same shape containing the same data.
    """

    return x.numpy()


def tensor_sumproducts(x: torch.Tensor, y: torch.Tensor):
    """
    x is an n-dimensional pytorch Tensor.
    y is an n-dimensional pytorch Tensor.

    Return the sum of the element-wise product of the two tensors.
    """

    return (x*y).sum()


def tensor_ReLU(x: torch.Tensor):
    """
    x is a pytorch Tensor. 
    For every element i in x, apply the ReLU function: 
    RELU(i) = 0 if i < 0 else i

    Return a pytorch Tensor of the same shape as x containing RELU(x)
    """

    return x.clamp(min=0)


def tensor_ReLU_prime(x):
    """
    x is a pytorch Tensor. 
    For every element i in x, apply the RELU_PRIME function: 
    RELU_PRIME(i) = 0 if i < 0 else 1

    Return a pytorch Tensor of the same shape as x containing RELU_PRIME(x)
    """

    x = x.clone()
    x[x == 0] = 1
    return x.clamp(min=0, max=1)
