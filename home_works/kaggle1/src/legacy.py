import time
import os
import sys
from datetime import datetime as dt
import pickle as pkl

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import copy

from hw0 import pad_pattern_end, pad_constant_central

import torch
from torch import nn, optim
from torch.utils.data import SequentialSampler, DataLoader
from data_utils import MyDataset, MyBatchSampler, WSJ, split_train_test

RESULTS_PATH = '../results'
DATA_PATH = '../data'
EXPERIMENT_DATETIME = str(dt.now()).split('.')[0] \
    .replace('-', '_') \
    .replace(' ', '_') \
    .replace(':', '_')

TOTAL_CLASSES = 138
FEATURES_PER_FRAME = 40

# set run mode if given
params = sys.argv
MODE = 'dev' if len(params) < 2 else params[1]
NUM_EPOCHS = 1 if MODE=='dev' else 20
print(f'[DEBUG] Running in a {MODE} mode')

def train_model(model, criterion, optimizer, scheduler=None, num_epochs=25):
    since = time.time()

    best_model_wts = copy.deepcopy(model.state_dict())
    best_acc = 0.0

    # storing metrics
    train_stats = {'train': [],
                   'val': []}

    for epoch in range(num_epochs):
        print('Epoch {}/{}'.format(epoch, num_epochs - 1))
        print('-' * 10)

        # Each epoch has a training and validation phase
        for phase in ['train', 'val']:
            if phase == 'train':
                if scheduler:
                    scheduler.step()
                model.train()  # Set model to training mode
            else:
                model.eval()  # Set model to evaluate mode

            running_loss = 0.0
            running_corrects = 0

            # Iterate over data.
            for inputs, labels in data_loaders[phase]:
                inputs = inputs.to(device)
                labels = labels.to(device)

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    outputs = model(inputs)
                    _, preds = torch.max(outputs, 1)
                    loss = criterion(outputs, labels)

                    # backward + optimize only if in training phase
                    if phase == 'train':
                        loss.backward()
                        optimizer.step()

                # statistics
                running_loss += loss.item() * inputs.size(0)
                running_corrects += torch.sum(preds == labels.data)

            epoch_loss = running_loss / dataset_sizes[phase]
            epoch_acc = running_corrects.double() / dataset_sizes[phase]

            # TODO chechkout
            train_stats[phase].append((epoch, epoch_loss, epoch_acc.item()))
            print('{} Loss: {:.4f} Acc: {:.4f}'.format(
                phase, epoch_loss, epoch_acc))

            # deep copy the model
            if phase == 'val' and epoch_acc > best_acc:
                best_acc = epoch_acc
                best_model_wts = copy.deepcopy(model.state_dict())

        print()

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(
        time_elapsed // 60, time_elapsed % 60))
    print('Best val Acc: {:4f}'.format(best_acc))

    # load best model weights
    model.load_state_dict(best_model_wts)
    return model, train_stats


def plot_spectrogram(dataset, n_spects2plot):
    """
    Shows plots of spectrogram listed in dataset X

    Args:
        dataset (object) - dataset entity
        n_spects2plot (int) - number spectrogram to show
    """

    # TODO add labels somehow on plot
    fig = plt.figure(figsize=(30, 10))
    for i in range(len(dataset)):
        sample = {'X': dataset.X_init,
                  'y': dataset.y_init}

        print(i, sample['X'].shape, sample['y'].shape)

        ax = plt.subplot(4, 1, i + 1)
        # plt.tight_layout()
        ax.set_title('Sample #{}'.format(i))
        # ax.axis('off')
        sns.heatmap(sample['X'].T)

        if i == n_spects2plot:
            plt.show()
            break

# TODO: cross-validate
FRAME_WINDOW = 8
BATCH_SIZE = 512
DROP_LAST = False
lr = 0.01
momentum = 0.9

print('[DEBUG] WSJ started')
wsj = WSJ(DATA_PATH)
print('[DEBUG] split started')
data_train, data_val = split_train_test(wsj.dev) if MODE == 'dev' else split_train_test(wsj.train)
print('[DEBUG] dataset build started')
dataset = {'train': MyDataset(data_train, frames2concat=FRAME_WINDOW),
           'val': MyDataset(data_val, frames2concat=FRAME_WINDOW)}

# TODO remove drop last later
print('[DEBUG] datasampler build started')
data_samplers = {x: MyBatchSampler(SequentialSampler(dataset[x]), batch_size=BATCH_SIZE, drop_last=DROP_LAST)
                 for x in ['train', 'val']}
print('[DEBUG] dataloader build started')
data_loaders = {x: DataLoader(dataset[x], batch_sampler=data_samplers[x], num_workers=4)
                for x in ['train', 'val']}
dataset_sizes = {x: len(dataset[x])
                 for x in ['train', 'val']}

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

model_mlp = nn.Sequential(
    nn.Linear(FRAME_WINDOW * FEATURES_PER_FRAME, 256),
    nn.ReLU(),
    nn.Linear(256, 256),
    nn.ReLU(),
    nn.Linear(256, 128),
    nn.ReLU(),
    nn.Linear(128, TOTAL_CLASSES))
model_mlp = model_mlp.to(device)

criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(model_mlp.parameters(), lr=lr, momentum=momentum)

print('[DEBUG] Modeling started')
model_mlp, metrics = train_model(model_mlp, criterion, optimizer, num_epochs=NUM_EPOCHS)

# saving results
torch.save(model_mlp.state_dict(), '{}/model_{}_{}.sav'.format(RESULTS_PATH, MODE, EXPERIMENT_DATETIME))
with open('{}/metrics{}.sav'.format(RESULTS_PATH, EXPERIMENT_DATETIME), 'wb') as f:
    pkl.dump(metrics, f)




# TODO make standalone module
# apply model on test data

# prepare data
phase = 'test'
X_test, _ = wsj.test
dataset[phase] =  MyDataset((X_test, None), frames2concat=FRAME_WINDOW)
sampler = SequentialSampler(dataset[phase])
data_samplers[phase] = MyBatchSampler(sampler=sampler, batch_size=BATCH_SIZE, drop_last=False)
data_loaders[phase] = DataLoader(dataset[phase], batch_sampler=data_samplers[phase], num_workers=4)

# create container
model_mlp.eval()
prediction = None

for inputs, labels in data_loaders[phase]:
    inputs = inputs.to(device)
    with torch.set_grad_enabled(False):
        outputs = model_mlp(inputs)
        _, preds = torch.max(outputs, 1)
        prediction = np.append(prediction, preds.numpy()) if prediction is not None else preds.numpy()

import pandas as pd
df = pd.DataFrame(prediction, index=np.arange(0, len(prediction))).reset_index()
df.columns = ['id', 'label']

df.to_csv('{}/submission_{}_{}.csv'.format(RESULTS_PATH, MODE, EXPERIMENT_DATETIME), index=False)
