import copy
import pickle as pkl
import sys
import time
from datetime import datetime as dt
from pprint import pprint

import torch
from torch import nn, optim
from torch.utils.data import DataLoader

from configs import DATA_PATH, RESULTS_PATH, TOTAL_CLASSES, FEATURES_PER_FRAME
from data_utils import MyDataset, WSJ, split_train_test
from model import Model


def train_model(model, criterion, optimizer, num_epochs, scheduler=None, data_loaders=None, dataset_sizes=None):
    since = time.time()

    best_model_wts = copy.deepcopy(model.state_dict())
    best_acc = 0.0

    # storing metrics
    train_stats = {'train': [],
                   'val': []}

    for epoch in range(num_epochs):
        print('Epoch {}/{}'.format(epoch, num_epochs - 1))
        print('-' * 10)

        # Each epoch has a training and validation phase
        for phase in ['train', 'val']:
            if phase == 'train':
                if scheduler:
                    scheduler.step()
                model.train()  # Set model to training mode
            else:
                model.eval()  # Set model to evaluate mode

            running_loss = 0.0
            running_corrects = 0

            # Iterate over data.
            for inputs, labels in data_loaders[phase]:
                inputs = inputs.to(DEVICE)
                labels = labels.to(DEVICE)

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    outputs = model(inputs)
                    _, preds = torch.max(outputs, 1)
                    loss = criterion(outputs, labels)

                    # backward + optimize only if in training phase
                    if phase == 'train':
                        loss.backward()
                        optimizer.step()

                # statistics
                running_loss += loss.item() * inputs.size(0)
                running_corrects += torch.sum(preds == labels.data)

            epoch_loss = running_loss / dataset_sizes[phase]
            epoch_acc = running_corrects.double() / dataset_sizes[phase]

            # TODO chechkout UX
            train_stats[phase].append((epoch, epoch_loss, epoch_acc.item()))
            print('{} Loss: {:.4f} Acc: {:.4f}'.format(
                phase, epoch_loss, epoch_acc))

            # deep copy the model
            if phase == 'val' and epoch_acc > best_acc:
                best_acc = epoch_acc
                best_model_wts = copy.deepcopy(model.state_dict())

        print()

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(
        time_elapsed // 60, time_elapsed % 60))
    print('Best val Acc: {:4f}'.format(best_acc))

    # load best model weights
    model.load_state_dict(best_model_wts)
    return model, train_stats





def main(mode='dev', experiment_name='test',
         frame_window=8,
         num_epochs=1, batch_size=512, lr=0.01, momentum=0.9,
         num_loader_workers=4):
    """
    Wrapper to training routine

    Args:
        mode: needed for choosing dataset (see `wsj`)
        experiment_name: name for saving results
        frame_window: n frames to concat near observed frame
        num_epochs:
        batch_size:
        lr:
        momentum:
        num_loader_workers:

    Returns:

    """
    wsj = WSJ(DATA_PATH)
    data_train, data_val = split_train_test(wsj.dev) if mode == 'dev' else split_train_test(wsj.train)
    dataset = {'train': MyDataset(data_train, frames2concat=frame_window),
               'val': MyDataset(data_val, frames2concat=frame_window)}

    data_loaders = {x: DataLoader(dataset[x], batch_size=batch_size, shuffle=True,
                                  drop_last=False, num_workers=num_loader_workers)
                    for x in ['train', 'val']}
    dataset_sizes = {x: len(dataset[x])
                     for x in ['train', 'val']}

    model_mlp = Model(frame_window * FEATURES_PER_FRAME, TOTAL_CLASSES).model
    model_mlp = model_mlp.to(DEVICE)

    criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(model_mlp.parameters(), lr=lr, momentum=momentum)

    model_mlp, metrics = train_model(model_mlp, criterion, optimizer, num_epochs,
                                     data_loaders=data_loaders, dataset_sizes=dataset_sizes)

    # saving results
    torch.save(model_mlp.state_dict(), '{}/model_{}_{}.pt'.format(RESULTS_PATH, mode, experiment_name))
    with open('{}/metrics_{}.sav'.format(RESULTS_PATH, experiment_name), 'wb') as f:
        pkl.dump(metrics, f)


DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

experiment_start_time = str(dt.now()).split('.')[0] \
    .replace('-', '_') \
    .replace(' ', '_') \
    .replace(':', '_')

# set run mode if given
params = sys.argv
run_params = {
    'experiment_name': experiment_start_time,
    'mode': 'dev',
    'frame_window': 8,
    'num_epochs': 1,
    'num_loader_workers': 4}

if len(params) > 1:
    run_params['mode'] = params[1]
    for param, param_key in zip(params[2:], list(run_params.keys())[2:]):
        run_params[param_key] = int(param)

# run baby, run
print('Running with config:')
pprint(run_params)
print()

main(**run_params)
