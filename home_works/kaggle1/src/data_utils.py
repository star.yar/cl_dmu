import os

import numpy as np
import torch
from torch.utils.data import BatchSampler, Dataset, SequentialSampler


class WSJ(object):
    """ Load the WSJ speech dataset

        Ensure dump_path is path to directory containing
        all data files (.npy) provided on Kaggle.

        Example usage:
            loader = WSJ()
            trainX, trainY = loader.train
            assert(trainX.shape[0] == 24590)

    """

    def __init__(self, numpy_dump_path):
        self.dev_set = None
        self.train_set = None
        self.test_set = None
        self.dump_path = numpy_dump_path

    @property
    def dev(self):
        if self.dev_set is None:
            self.dev_set = WSJ.load_raw(self.dump_path, 'dev')
        return self.dev_set

    @property
    def train(self):
        if self.train_set is None:
            self.train_set = WSJ.load_raw(self.dump_path, 'train')
        return self.train_set

    @property
    def test(self):
        if self.test_set is None:
            self.test_set = (np.load(os.path.join(self.dump_path, 'test.npy'), encoding='bytes'), None)
        return self.test_set

    @staticmethod
    def load_raw(path, name):
        return (
            np.load(os.path.join(path, '{}.npy'.format(name)), encoding='bytes'),
            np.load(os.path.join(path, '{}_labels.npy'.format(name)), encoding='bytes')
        )


class MyDataset(Dataset):
    """
    Custom CMU dataset
    Current idea behind forming dataset is to take phoneme by end of speach seq
    """

    def __init__(self, wsj_property, frames2concat, transform=None):
        """
        Args:
            wsj_property (object): WSJ object with preloaded numpy data
            frames2concat (int): Number of frames to concatenate nearby observed frame
            transform (callable, optional): Optional transform to be applied on a sample.
        """

        self.X, self.y = wsj_property
        self.k = frames2concat
        self.k_half = self.k // 2
        self.transform = transform

        # array of id tail for every utter
        self.frame_last_idx = []
        for utter in self.X:
            prev_size = self.frame_last_idx[-1] if len(self.frame_last_idx) > 0 else 0
            self.frame_last_idx.append(prev_size + utter.shape[0])
        self.frame_last_idx = np.asarray(self.frame_last_idx)

    def __len__(self):
        return self.frame_last_idx[-1]

    def __getitem__(self, idx):
        u_id = self.get_frame_utterance(idx)
        u_start = self.frame_last_idx[u_id - 1] if u_id > 0 else 0
        u_end = self.X[u_id].shape[0]
        frame_idx = idx - u_start

        # collect X with padding head and tail situations
        if frame_idx - self.k_half < 0:
            x = self.X[u_id][:frame_idx + self.k_half]
            x = np.pad(x, ((self.k - x.shape[0], 0), (0, 0)), 'minimum')
        elif frame_idx + self.k_half > u_end:
            x = self.X[u_id][frame_idx - self.k_half:]
            x = np.pad(x, ((0, self.k - x.shape[0]), (0, 0)), 'minimum')
        else:
            x = self.X[u_id][frame_idx - self.k_half:frame_idx + self.k_half]

        x = x.reshape(-1)

        sample = {'X': x,
                  'y': self.get_y(u_id, frame_idx)}

        return torch.tensor(sample['X']), \
               torch.tensor(sample['y']).long()

    def get_frame_utterance(self, idx):
        return np.where(self.frame_last_idx > idx)[0][0]

    def get_y(self, u_id, frame_idx):
        """
        This method helps to return proper values for test set

        Args:
            u_id (int): utter id
            frame_idx (int): frame id

        Returns:
            y (int): target value
        """
        if self.y is not None:
            return self.y[u_id][frame_idx].item()
        else:
            return 0


class MyBatchSampler(BatchSampler):
    """Wraps another sampler to yield a mini-batch of indices."""

    def __init__(self, sampler, batch_size, drop_last):
        """

        Args:
            sampler (Sampler): Base sampler.
            batch_size (int): Size of mini-batch.
            drop_last (bool): If ``True``, the sampler will drop the last batch if
                its size would be less than ``BATCH_SIZE``

        Example:
            >>> list(BatchSampler(SequentialSampler(range(10)), BATCH_SIZE=3, drop_last=False))
            [[0, 1, 2], [3, 4, 5], [6, 7, 8], [9]]
            >>> list(BatchSampler(SequentialSampler(range(10)), BATCH_SIZE=3, drop_last=True))
            [[0, 1, 2], [3, 4, 5], [6, 7, 8]]
        """
        super().__init__(sampler, batch_size, drop_last)
        self.get_utter = sampler.data_source.get_frame_utterance

    def __iter__(self):
        batch = []
        domain_utter = -1

        for idx in self.sampler:
            # load frames only from one utterance
            utter = self.get_utter(idx)

            # when found new utter
            if utter != domain_utter:
                if len(batch) > 0 and not self.drop_last:
                    yield batch
                domain_utter = utter
                batch = []

            batch.append(idx)

            # when reached batchsize
            if len(batch) == self.batch_size:
                yield batch
                batch = []

        # last element in sampler
        if len(batch) > 0 and not self.drop_last:
            yield batch

    def __len__(self):
        # TODO improve len evaluation somehow
        X = self.sampler.data_source.X

        if self.drop_last:
            return int(sum(np.floor(len(utter) / self.batch_size) for utter in X))
        else:
            return int(sum(np.ceil(len(utter) / self.batch_size) for utter in X))


def split_train_test(wsj_prop, test_size=0.33, random_state=42):
    X, y = wsj_prop

    # choose random utts
    size = int(X.shape[0] * test_size)
    np.random.seed(random_state)
    test_idxs = np.random.choice(np.arange(0, X.shape[0]), size, replace=False)
    t_test = X[test_idxs].copy(), y[test_idxs].copy()
    t_train = np.delete(X, test_idxs), np.delete(y, test_idxs)
    return t_train, t_test
