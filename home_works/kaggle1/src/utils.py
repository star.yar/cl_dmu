import pandas as pd
import numpy as np
import pickle as pkl

import matplotlib.pyplot as plt
import seaborn as sns


def plot_spectrogram(dataset, n_spects2plot):
    """
    Shows plots of spectrogram listed in dataset X

    Args:
        dataset (object) - dataset entity
        n_spects2plot (int) - number spectrogram to show
    """

    # TODO add labels somehow on plot
    fig = plt.figure(figsize=(30, 10))
    for i in range(len(dataset)):
        sample = {'X': dataset.X_init,
                  'y': dataset.y_init}

        print(i, sample['X'].shape, sample['y'].shape)

        ax = plt.subplot(4, 1, i + 1)
        # plt.tight_layout()
        ax.set_title('Sample #{}'.format(i))
        # ax.axis('off')
        sns.heatmap(sample['X'].T)

        if i == n_spects2plot:
            plt.show()
            break


def plot_learning_curves(filename):
    with open(filename, 'rb') as f:
        metrics = pkl.load(f)

    phases = list(metrics.keys())
    df = {phase: pd.DataFrame(metrics[phase], columns=['epoch', 'loss', 'acc'])
          for phase in phases}

    for metric_name in ['loss', 'acc']:
        plt.figure(figsize=(10, 5))
        plt.plot(df[phases[0]][metric_name])
        plt.plot(df[phases[1]][metric_name])
        plt.legend(phases)
        plt.show()

    return df


m = plot_learning_curves('../results/metrics_2019_03_17_18_04_03.sav')
print('Best val score: {}'.format(m['val']['acc'].max()))

