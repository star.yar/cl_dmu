import sys

import pandas as pd
import numpy as np

import torch
from torch.utils.data import DataLoader

from configs import DATA_PATH, RESULTS_PATH, TOTAL_CLASSES, FEATURES_PER_FRAME
from data_utils import WSJ, MyDataset
from model import Model

# load arguments
params = sys.argv
assert len(params) >= 3
experiment_name = params[1]
frame_window = int(params[2])

wsj = WSJ(DATA_PATH)
# prepare data
phase = 'test'
X_test, _ = wsj.test
dataset = MyDataset((X_test, None), frames2concat=frame_window)
data_loaders = DataLoader(dataset, batch_size=len(dataset) // 2, shuffle=False, drop_last=False, num_workers=4)

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

# load model wrapper
model = Model(frame_window * FEATURES_PER_FRAME, TOTAL_CLASSES).model
model.to(device)

# load pre-trained model
# TODO move to .pt format for newer models
model_path = '{}/model_prod_{}.sav'.format(RESULTS_PATH, experiment_name)
model.load_state_dict(torch.load(model_path))
model.eval()

prediction = None
for inputs, labels in data_loaders:
    inputs = inputs.to(device)
    with torch.set_grad_enabled(False):
        outputs = model(inputs)
        _, preds = torch.max(outputs, 1)
        preds = preds.cpu().numpy()
        prediction = np.append(prediction, preds) if prediction is not None else preds

df = pd.DataFrame(prediction, index=np.arange(0, len(prediction))).reset_index()
df.columns = ['id', 'label']

save_path = '{}/submission_{}.csv'.format(RESULTS_PATH, experiment_name)
df.to_csv(save_path, index=False)
print(f"Saved at {save_path}")
