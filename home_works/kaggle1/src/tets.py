# # EX3
# size = int(input())
#
# prev_val = None
# val = None
# last_printed = None
# different = False
#
# if size > 0:
#     for i in range(size):
#         val = input()
#         if prev_val is None:
#             prev_val = val
#
#         if val != prev_val:
#             print(prev_val)
#             last_printed = prev_val
#             prev_val = val
#
#     if last_printed is None or (val == prev_val and val != last_printed):
#         print(val)

# # EX2
# size = int(input())
#
# max_len = 0
# prev_val = None
# val = None
# ones_len = 0
#
# if size > 0:
#     for i in range(size):
#         val = int(input())
#         if prev_val is None:
#             prev_val = val
#
#         if val == 0:
#             ones_len = 0
#         else:
#             ones_len += 1
#             if ones_len>max_len:
#                 max_len = ones_len
#
# print(max_len)

# EX4


def generate_bracket(ind, n_opened, opens_used):
    if ind < size and opens_used < size:
        seq[ind] = "("
        if opens_used < size:
            generate_bracket(ind+1, n_opened+1, opens_used+1)

    if n_opened > 0:
        seq[ind] = ")"
        if ind < size * 2 and opens_used < size:
            generate_bracket(ind+1, n_opened-1, opens_used)

    if seq and ind == size * 2:
        print("".join(seq))


size = int(input())
seq = [None] * size * 2
generate_bracket(0, 0, 0)
