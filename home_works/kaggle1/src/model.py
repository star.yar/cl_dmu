from torch import nn


class Model(object):
    def __init__(self, in_features, out_features):
        self.model = nn.Sequential(
            nn.Linear(in_features, 1024),
            nn.ReLU(),
            nn.Linear(1024, 512),
            nn.ReLU(),
            nn.Linear(512, 256),
            nn.ReLU(),
            nn.Linear(256, 128),
            nn.ReLU(),
            nn.Linear(128, out_features))

    def __call__(self, *args, **kwargs):
        self.model(*args, **kwargs)
