import torch
import torch.nn as nn
import numpy as np
import itertools


class Sigmoid:
    """docstring for Sigmoid"""

    def __init__(self):
        pass

    def forward(self, x):
        self.res = 1 / (1 + np.exp(-x))
        return self.res

    def backward(self):
        return self.res * (1 - self.res)

    def __call__(self, x):
        return self.forward(x)


class Tanh:
    def __init__(self):
        pass

    def forward(self, x):
        self.res = np.tanh(x)
        return self.res

    def backward(self):
        return 1 - (self.res ** 2)

    def __call__(self, x):
        return self.forward(x)


class GRU_Cell:
    """docstring for GRU_Cell"""

    def __init__(self, in_dim, hidden_dim):
        self.d = in_dim
        self.h = hidden_dim
        h = self.h
        d = self.d

        self.Wzh = np.random.randn(h, h)
        self.Wrh = np.random.randn(h, h)
        self.Wh = np.random.randn(h, h)

        self.Wzx = np.random.randn(h, d)
        self.Wrx = np.random.randn(h, d)
        self.Wx = np.random.randn(h, d)

        self.dWzh = np.zeros((h, h))
        self.dWrh = np.zeros((h, h))
        self.dWh = np.zeros((h, h))

        self.dWzx = np.zeros((h, d))
        self.dWrx = np.zeros((h, d))
        self.dWx = np.zeros((h, d))

        self.z_act = Sigmoid()
        self.r_act = Sigmoid()
        self.h_act = Tanh()

        self._x = None
        self._h = None
        self.z = None
        self.r = None
        self.n = None
        self.ht = None

    def forward(self, x, h):
        # input:
        # 	- x: shape(input dim),  observation at current time-step
        # 	- h: shape(hidden dim), hidden-state at previous time-step
        #
        # output:
        # 	- h_t: hidden state at current time-step

        x = x.reshape(-1, 1)
        h = h.reshape(-1, 1)

        z = self.z_act(self.Wzh @ h + self.Wzx @ x)
        r = self.r_act(self.Wrh @ h + self.Wrx @ x)
        n = self.h_act(self.Wh @ (r * h) + self.Wx @ x)
        ht = (1 - z) * h + z * n

        self._x, self._h, self.z, self.r, self.n, self.ht = x, h, z, r, n, ht
        return ht.reshape(-1)

    def backward(self, dl_dht):
        # input:
        # 	- dl_dht: 	shape(hidden dim), summation of derivative wrt loss from next layer at
        # 			same time-step and derivative wrt loss from same layer at
        # 			next time-step
        #
        # output:
        # 	- dx: 	Derivative of loss wrt the input x
        # 	- dh: 	Derivative of loss wrt the input hidden h

        if [x for x in [self._x, self._h, self.z, self.r, self.n, self.ht] if x is None]:
            raise AttributeError

        # # Autolab DEBUG part
        # print('dl_dht.shape: {}'.format(dl_dht.shape))
        # print('self._x.shape: {}'.format(self._x.shape))
        # print('self._h.shape: {}'.format(self._h.shape))

        dl_dht = dl_dht.reshape(1, -1)

        # 1. differentiate wrt Wzh, Wzx
        dl_dz = dl_dht * (- self._h + self.n).T
        dl_dz_affine = dl_dz * self.z_act.backward().T
        self.dWzh += (self._h @ dl_dz_affine).T
        self.dWzx += (self._x @ dl_dz_affine).T

        # 2. differentiate wrt Wrh, Wrx
        dl_dn = dl_dht * self.z.T
        dl_dn_affine = dl_dn * self.h_act.backward().T
        dl_dr = dl_dn_affine @ self.Wh * self._h.T
        dl_dr_affine = dl_dr * self.r_act.backward().T
        self.dWrh += (self._h @ dl_dr_affine).T
        self.dWrx += (self._x @ dl_dr_affine).T

        # 3. differentiate wrt Wh, Wx
        self.dWh += ((self.r * self._h) @ dl_dn_affine).T
        self.dWx += (self._x @ dl_dn_affine).T

        # 4. differentiate  wrt input
        dx = (dl_dz_affine @ self.Wzx
              + dl_dr_affine @ self.Wrx
              + dl_dn_affine @ self.Wx)
        dh = (dl_dht * (1 - self.z).T
              + dl_dz_affine @ self.Wzh
              + dl_dr_affine @ self.Wrh
              + dl_dn_affine @ self.Wh * self.r.T)

        return dx, dh


def test():
    np.random.seed(1)

    seq_len = 10
    in_dim = 5
    hidden_dim = 2

    data = np.random.randn(seq_len, in_dim)
    hidden = np.random.randn(hidden_dim)

    g_stud = GRU_Cell(in_dim, hidden_dim)
    g_sol = nn.GRUCell(in_dim, hidden_dim)
    params = g_sol.state_dict()
    params.values()

    # h_sol = g_sol.forward(torch.tensor(x).float(),
    #                       torch.tensor(h).float())
    h_stud = g_stud.forward(data[0], hidden)

    # assert h_sol.shape == h_stud.shape
    # eq_elems = h_sol.detach().numpy() == h_stud
    # assert eq_elems.all()

    delta = np.random.randn(hidden_dim).reshape(1, -1)
    d_x, d_h = g_stud.backward(delta)


if __name__ == '__main__':
    test()
