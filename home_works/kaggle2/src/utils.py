import sys
from datetime import datetime

import pickle as pkl
import pandas as pd
from sklearn.metrics import roc_auc_score
import torch


def get_auc(y_true, y_score):
    auc = roc_auc_score(y_true, y_score)
    return auc


def get_auc_cli():
    args = sys.argv[1:]
    if len(args) < 2:
        print("python score.py <score_csv> <label_csv>")
        exit()
    score_csv = args[0]
    label_csv = args[1]
    y_score = pd.read_csv(score_csv).score.values
    y_true = pd.read_csv(label_csv).score.values
    auc = get_auc(y_true, y_score)
    print("AUC: ", auc)


def pkl_save(o, path):
    with open(path, 'wb') as f:
        pkl.dump(o, f)


def pkl_load(path):
    with open(path, 'rb') as f:
        return pkl.load(f)


def get_datetime_now():
    return str(datetime.now()).split('.')[0] \
        .replace('-', '_') \
        .replace(' ', '_') \
        .replace(':', '_')


def log(epoch, logger, scalars, model, phase='train'):
    """
    Log metrics and model params

    Args:
        phase (str): one of ['train', 'val'], model logging will be done only for train phase
        epoch (int): train epoch
        logger (SummaryWriter): logger
        scalars (dict): pass in format: {metric_name: metric_val}
        model (nn.Module): torch model
    """

    for name, val in scalars.items():
        logger.add_scalar(name, val, epoch)

    if phase == 'train':
        # log model params
        for tag, value in model.named_parameters():
            tag = tag.replace('.', '/')
            logger.add_histogram(tag, get_item(value.data), epoch + 1)
            if value.grad is not None:
                # We need this since last layer is chosen ad-hoc.
                # For example, if we train classifier,
                # there will be no grad data at validator.
                logger.add_histogram(tag + '/grad', get_item(value.grad.data), epoch + 1)


def get_item(parameter: torch.Tensor):
    return parameter.cpu().numpy()


def load_checkpoint(path, model=None, opt=None, device='cpu'):
    """
    Loads model from ckp if model instance is not passed.
    Loads model weights from ckp.
    Loads state from ckp for opt if opt is passed.

    Args:
        model (torch.nn.Module): Model instance to load weights to
        opt (object): Opt instance to load weights to
        path (str): Checkpoint path
        device (str): `cuda:id` or `cpu`
    """

    passed_model = False if model is None else True
    passed_opt = False if opt is None else True

    checkpoint_data = torch.load(path, map_location=device)

    if 'epoch' in checkpoint_data.keys():
        # print last epoch number
        epoch = checkpoint_data['epoch']
        print(f'[DEBUG] Started from epoch #{epoch}')

    # create model if not passed
    if not passed_model:
        print('[WARNING] Loading model from .sav is not safe!')
        model = checkpoint_data['model']

    # load model state
    model.load_state_dict(checkpoint_data['model_wts'])
    model.to(device)

    # load optimizer params if passed
    if passed_opt:
        opt_state = checkpoint_data['optimizer']
        opt.load_state_dict(opt_state)
        print('[DEBUG] Loaded optimizer with params')

    if not passed_model:
        return model


def checkpoint(epoch, args, model, optimizer, train_stats, path, best_model_wts=None):
    torch.save({
        'epoch': epoch,
        'params': args,
        'model': model,  # saving model binaries
        'model_wts': model.state_dict(),
        'optimizer': optimizer.state_dict(),
        'metrics': train_stats,
        'best_model_wts': best_model_wts
    }, path)
