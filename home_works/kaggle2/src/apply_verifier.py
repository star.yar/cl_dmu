import numpy as np
import pandas as pd
import torch
from torch.utils.data import DataLoader

from data_utils import TestFaceDataset, build_transformers, TestSampler
from utils import get_item
from configs import \
    EXPERIMENT_NAME, \
    DEVICE, \
    TEST_BATCH_SIZE, \
    DATA_PATH, TEST_PATH, RESULTS_PATH, \
    IMG_SIZE, \
    WORKERS

# prepare data
dataset = TestFaceDataset(TEST_PATH, transform=build_transformers('test', IMG_SIZE))
data_loader = DataLoader(dataset, sampler=TestSampler('{}/test_verification.txt'.format(DATA_PATH)),
                         batch_size=TEST_BATCH_SIZE, shuffle=False, drop_last=False, num_workers=WORKERS)

# load model
experiment = torch.load('{}/experiment_{}.pt'.format(RESULTS_PATH, EXPERIMENT_NAME))
model = experiment['model']
model.to(DEVICE)
model.eval()

prediction = None
for inputs in data_loader:
    inputs = inputs.to(DEVICE)
    with torch.set_grad_enabled(False):
        outputs = model(inputs)
        similarity_score, preds = torch.max(outputs, 1)
        # load and detach
        similarity_score, preds = get_item(similarity_score), get_item(preds)
        # save results
        list_of_rows = np.append(prediction, similarity_score, preds) if prediction is not None else preds

df = pd.DataFrame(list_of_rows, index=np.arange(0, len(list_of_rows))).reset_index()
df.columns = ['id', 'score', 'label']

save_path = '{}/valid_scores_{}.csv'.format(RESULTS_PATH, EXPERIMENT_NAME)
df.to_csv(save_path, index=False)
print(f"Saved at {save_path}")

# load comparisons
order = pd.read_csv(f'{DATA_PATH}/order_validation.csv', index_col=0)
order = order \
    .merge(df, left_on='img1', right_index=True, suffixes=('', '_img1')) \
    .merge(df, left_on='img2', right_index=True, suffixes=('', '_img2'))

order['diff'] = order['img1'] - order['img2']
order[['diff']].to_csv(f'{RESULTS_PATH}/valid_submission_{EXPERIMENT_NAME}.csv')
