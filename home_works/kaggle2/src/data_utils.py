import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import torch
from PIL import Image
from torch.utils.data import Dataset, Sampler
from torchvision import transforms

from utils import get_item


def count_totals_pics(data_path):
    return sum([len(os.listdir(f'{data_path}/{name}'))
                for name in os.listdir(data_path)
                if not name.startswith('.')])


def count_unique_faces(data_path):
    return len([name
                for name in os.listdir(data_path)
                if not name.startswith('.')])


def load_img_folder_tree(data_path):
    return [(int(name), np.array(sorted(os.listdir(f'{data_path}/{name}'))))
            for name in sorted(os.listdir(data_path))
            if not name.startswith('.')]


def show(img):
    plt.imshow(img)
    plt.show()


def show_batch(in_batch):
    # get input array
    bs = in_batch.shape[0]
    in_batch = get_item(in_batch)

    # tmp normalization
    in_batch = in_batch / in_batch.max()

    import seaborn as sns
    sb_size = 20
    sb_num = int(np.ceil(len(in_batch) / sb_size))
    for sb_id in range(sb_num):
        start_id, end_id = (sb_size * sb_id, sb_size * (sb_id + 1))
        sb = in_batch[start_id:end_id]

        # plot settings
        horizontal = 5
        vertical = int(np.ceil(sb_size / horizontal))

        plt.figure(figsize=(20, 20))
        plt.axis('off')
        for elem_id in range(len(sb)):
            plt.subplot(vertical, horizontal, elem_id + 1)
            sns.distplot(sb[elem_id].flatten())
        plt.show()

    # plot settings
    horizontal = 4
    vertical = int(np.ceil(bs / horizontal))

    plt.figure(figsize=(20, 20))
    plt.axis('off')
    for elem_id in range(bs):
        plt.subplot(vertical, horizontal, elem_id + 1)
        data = in_batch[elem_id].transpose(1, 2, 0)
        data = (data - data.min()) / (data.max() - data.min())
        plt.imshow(data, vmin=0, vmax=1)
    plt.show()


def build_indices(array):
    """
    Builds an array of last id for every utter

    Args:
        array (array-like): An array of arrays of volatile sizes
    """
    last_indices = []
    for utter_id, utter in array:
        prev_size = last_indices[-1] if len(last_indices) > 0 else 0
        last_indices.append(prev_size + len(utter))

    return np.asarray(last_indices)


def idx2utter(u_getter, tail_idx_array, idx):
    u_id = u_getter(tail_idx_array, idx)
    u_start = tail_idx_array[u_id - 1] if u_id > 0 else 0
    u_end = tail_idx_array[u_id]  # optional staff
    return u_id, idx - u_start


def get_img_folder(tail_idx_array, idx):
    return np.where(tail_idx_array > idx)[0][0]


class FaceDataset(Dataset):
    """Face dataset for CMU kaggle2."""

    def __init__(self, root_dir, transform=None):
        """
        Args:
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied on a sample.
        """
        self.img_folder_tree = load_img_folder_tree(root_dir)
        self.tail_idx_array = build_indices(self.img_folder_tree)
        self.root_dir = root_dir
        self.transform = transform

    def __len__(self):
        return self.tail_idx_array[-1]

    def __getitem__(self, idx):
        folder_idx, img_idx = idx2utter(get_img_folder, self.tail_idx_array, idx)
        face_id, images = self.img_folder_tree[folder_idx]
        img_name = images[img_idx]
        image = Image.open(f'{self.root_dir}/{face_id}/{img_name}')

        if self.transform:
            image = self.transform(image)

        return image, torch.tensor(face_id).long()


class TestFaceDataset(Dataset):
    """Face dataset for CMU kaggle2."""

    def __init__(self, root_dir, channels_first=False, transform=None):
        """
        Args:
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied on a sample.
        """

        self.root_dir = root_dir
        self.n_images = len(os.listdir(self.root_dir))
        self.transform = transform
        self.channels_first = channels_first

    def __len__(self):
        return self.n_images

    def __getitem__(self, img_name):
        image = Image.open(f'{self.root_dir}/{img_name}')

        if self.transform:
            image = self.transform(image)

        return image


class TestSampler(Sampler):
    def __init__(self, order_file_path):
        self.order_file = order_file_path
        self.order = pd.read_csv(order_file_path, header=None, names=['img_name'])['img_name'].values

    def __iter__(self):
        return iter(self.order)

    def __len__(self):
        return len(self.order)


def build_transformers(phase, pic_size):
    # base = [transforms.Resize(pic_size)] if pic_size > 32 else []
    base = [transforms.Pad((int((pic_size - 32) / 2), int((pic_size - 32) / 2)))] if pic_size > 32 else []

    normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                     std=[0.229, 0.224, 0.225])

    tfs = {
        'train': transforms.Compose(base + [
            # transforms.RandomResizedCrop(pic_size, scale=(0.5, 1.0), ratio=(0.75, 1.3333333333333333)),
            transforms.RandomRotation(45),
            transforms.ColorJitter(0.25, 0.25),
            transforms.ToTensor(),
            normalize
        ]),

        'val': transforms.Compose(base + [
            # transforms.RandomResizedCrop(pic_size, scale=(0.8, 1.0), ratio=(0.75, 1.3333333333333333)),
            transforms.RandomRotation(30),
            transforms.ToTensor(),
            normalize
        ]),

        'test': transforms.Compose(base + [
            transforms.ToTensor(),
            normalize
        ])
    }
    return tfs[phase]


if __name__ == '__main__':
    # some tests
    pass

    # # test dataset
    # train_data = FaceDataset(TRAIN_PATH)
    # img, label = train_data[0]
    # show(img)
    # print(f'Data len: {len(train_data)}')
    #
    # img_folder = ImageFolder(TRAIN_PATH)
    # img_, class_ = img_folder[0]
    # show(img_)
    # print(f'Data len: {len(img_folder)}')

    # # test TestFaceDataset
    # test_data = TestFaceDataset(get_data_paths('medium'))
    # img = test_data['0.jpg']
    # show(img)
    # print()
    #
    # # test TestSampler
    # sampler = TestSampler(DATA_PATH_ROOT + '/test_order_classification.txt')
    # len(sampler)
    # for i, elem in enumerate(sampler):
    #     show(test_data[elem][0])
    #     if i == 2:
    #         break
