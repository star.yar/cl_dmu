import copy
import time

import torch
from tensorboardX import SummaryWriter
from torch import nn
from torch import optim
from torch.utils.data import DataLoader

from data_utils import build_transformers, show_batch, FaceDataset
from models import get_trainable_parameters, MobileNetV2 as DefaultModel, AngleLoss, AngleLinear
from utils import pkl_save, log, load_checkpoint, checkpoint


def train_model(model, criterion, optimizer, num_epochs, scheduler=None, data_loaders=None, dataset_sizes=None,
                loggers=None, sub_loggers=None):
    since = time.time()

    best_model_wts = copy.deepcopy(model.state_dict())
    best_model_optimizer = copy.deepcopy(optimizer.state_dict())
    best_acc = 0.0

    # storing metrics
    train_stats = {x: []
                   for x in ['train', 'val']}

    for epoch in range(START_EPOCH, num_epochs):
        print('Epoch {}/{}'.format(epoch + 1, num_epochs))
        print('-' * 10)

        # Each epoch has a training and validation phase
        for phase in ['train', 'val']:
            # init metric vars on start
            i, seen = 0, 0
            running_loss, running_corrects = .0, 0

            if phase == 'train':
                if scheduler:
                    scheduler.step()  # for everything except plateau
                model.train()  # Set model to training mode
            else:
                model.eval()  # Set model to evaluate mode

            # Iterate over data.
            for inputs, labels in data_loaders[phase]:
                inputs = inputs.to(DEVICE)
                labels = labels.to(DEVICE)

                if SHOW_BATCH:
                    show_batch(inputs)

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    outputs = model(inputs, TASK)
                    loss = criterion(outputs, labels)
                    # TODO: this is needed scince model returns (cos, phi); Do remove latter
                    if TASK == 'valid':
                        outputs = outputs[0]
                    _, preds = torch.max(outputs, 1)

                    # backward + optimize only if in training phase
                    if phase == 'train':
                        loss.backward()
                        optimizer.step()

                # statistics
                running_loss += loss.item() * inputs.size(0)
                running_corrects += torch.sum(preds == labels.data)

                if sub_loggers and (i == 0 or i % MICRO_LOG_EVERY == 0):
                    i += 1
                    seen += inputs.size(0)
                    log(i + epoch * len(data_loaders),
                        sub_loggers[phase],
                        {'Loss': running_loss / seen,
                         'Accuracy': (running_corrects.double() / seen).item()},
                        model,
                        phase=phase)

            epoch_loss = running_loss / dataset_sizes[phase]
            epoch_acc = running_corrects.double() / dataset_sizes[phase]

            # saving stats
            train_stats[phase].append((epoch, epoch_loss, epoch_acc.item()))
            print('{} Loss: {:.4f} Acc: {:.4f}'.format(
                phase, epoch_loss, epoch_acc))
            log(epoch,
                loggers[phase],
                {'Loss': epoch_loss, 'Accuracy': epoch_acc.item()},
                model,
                phase=phase)

            # deep copy the model
            if phase == 'val' and epoch_acc > best_acc:
                best_acc = epoch_acc
                best_model_wts = copy.deepcopy(model.state_dict())

        if epoch % CP_EVERY == 0:
            checkpoint(epoch, args, model, optimizer, train_stats,
                       '{}/experiment_{}_{}.pt'.format(CP_PATH, EXPERIMENT_NAME, epoch),
                       best_model_wts=best_model_wts)

        print()

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(
        time_elapsed // 60, time_elapsed % 60))
    print('Best val Acc: {:4f}'.format(best_acc))

    # load best model weights
    model.load_state_dict(best_model_wts)
    optimizer.load_state_dict(best_model_optimizer)
    return model, optimizer, train_stats


def main():
    """
    Wrapper to training routine
    """

    micro_loggers = {x: SummaryWriter('{}/micro_{}_{}'.format(LOGS_PATH, EXPERIMENT_NAME, x))
                     for x in ['train', 'val']} if MICRO_LOG_EVERY else None
    loggers = {x: SummaryWriter('{}/{}_{}'.format(LOGS_PATH, EXPERIMENT_NAME, x))
               for x in ['train', 'val']}

    dataset = {x: FaceDataset({'train': TRAIN_PATH, 'val': VAL_PATH}[x],
                              transform=build_transformers(x, IMG_SIZE))
               for x in ['train', 'val']}
    data_loaders = {x: DataLoader(dataset[x], batch_size=TRAIN_BATCH_SIZE, shuffle=True,
                                  drop_last=False, num_workers=WORKERS)
                    for x in ['train', 'val']}
    dataset_sizes = {x: len(dataset[x])
                     for x in ['train', 'val']}

    # TMP: create, inspect and save model
    model = DefaultModel(IMG_SIZE, NUM_CLASSES)
    print(f'[DEBUG] Total trainable parameters: {sum(get_trainable_parameters(model))}')
    pkl_save(model, '{}/model_{}.sav'.format(RESULTS_PATH, EXPERIMENT_NAME))
    model = model.to(DEVICE)

    criterion = {'class': nn.CrossEntropyLoss(),
                 'valid': AngleLoss()}[TASK]
    optimizer = optim.RMSprop(model.parameters(), lr=LR,
                              momentum=MOMENTUM, weight_decay=WEIGHT_DECAY)
    scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=STEP, gamma=GAMMA)

    # loading existing checkpoint
    # TODO: watch and simplify this part when possible
    if RESUME:
        if LOAD_OPT:
            load_checkpoint(RESUME, model=model, opt=optimizer, device=DEVICE)
        else:
            load_checkpoint(RESUME, model=model, device=DEVICE)

    # is passed path then in priority compared to resume setting
    if PRETRAINED_PATH:
        model = torch.load(PRETRAINED_PATH)

    model, optimizer, metrics = train_model(model, criterion, optimizer, START_EPOCH + EPOCHS,
                                            scheduler=scheduler,
                                            data_loaders=data_loaders, dataset_sizes=dataset_sizes,
                                            loggers=loggers, sub_loggers=micro_loggers)

    # saving results
    checkpoint(EPOCHS, args, model, optimizer, metrics,
               '{}/experiment_{}.pt'.format(RESULTS_PATH, EXPERIMENT_NAME))


if __name__ == '__main__':
    from configs import args, \
        DEVICE, \
        TASK, \
        PRETRAINED_PATH, CP_PATH, RESUME, CP_EVERY, LOAD_OPT, \
        RESULTS_PATH, LOGS_PATH, TRAIN_PATH, VAL_PATH, \
        START_EPOCH, EPOCHS, \
        MICRO_LOG_EVERY, EXPERIMENT_NAME, \
        IMG_SIZE, NUM_CLASSES, \
        TRAIN_BATCH_SIZE, WORKERS, \
        LR, MOMENTUM, WEIGHT_DECAY, STEP, GAMMA, \
        SHOW_BATCH

    # run baby, run
    print('Running with config')
    print('-' * 10)
    for key, val in sorted(args.items()):
        print('{}: {}'.format(key, val))
    print()

    # save session params
    pkl_save(args, '{}/params_{}.sav'.format(RESULTS_PATH, EXPERIMENT_NAME))

    # main routine
    main()
