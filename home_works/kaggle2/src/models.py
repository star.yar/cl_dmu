import math

import torch
import torchvision
from torch import nn
from torch.autograd import Variable
from torch.nn import functional as F, Parameter


def conv_bn(inp, oup, stride):
    return nn.Sequential(
        nn.Conv2d(inp, oup, 3, stride, 1, bias=False),
        nn.BatchNorm2d(oup),
        nn.ReLU6(inplace=True)
    )


def conv_1x1_bn(inp, oup):
    return nn.Sequential(
        nn.Conv2d(inp, oup, 1, 1, 0, bias=False),
        nn.BatchNorm2d(oup),
        nn.ReLU6(inplace=True)
    )


class InvertedResidual(nn.Module):
    def __init__(self, inp, oup, stride, expand_ratio):
        super(InvertedResidual, self).__init__()
        self.stride = stride
        assert stride in [1, 2]

        hidden_dim = round(inp * expand_ratio)
        self.use_res_connect = self.stride == 1 and inp == oup

        if expand_ratio == 1:
            self.conv = nn.Sequential(
                # dw
                nn.Conv2d(hidden_dim, hidden_dim, 3, stride, 1, groups=hidden_dim, bias=False),
                nn.BatchNorm2d(hidden_dim),
                nn.ReLU6(inplace=True),
                # pw-linear
                nn.Conv2d(hidden_dim, oup, 1, 1, 0, bias=False),
                nn.BatchNorm2d(oup),
            )
        else:
            self.conv = nn.Sequential(
                # pw
                nn.Conv2d(inp, hidden_dim, 1, 1, 0, bias=False),
                nn.BatchNorm2d(hidden_dim),
                nn.ReLU6(inplace=True),
                # dw
                nn.Conv2d(hidden_dim, hidden_dim, 3, stride, 1, groups=hidden_dim, bias=False),
                nn.BatchNorm2d(hidden_dim),
                nn.ReLU6(inplace=True),
                # pw-linear
                nn.Conv2d(hidden_dim, oup, 1, 1, 0, bias=False),
                nn.BatchNorm2d(oup),
            )

    def forward(self, x):
        if self.use_res_connect:
            return x + self.conv(x)
        else:
            return self.conv(x)


class MobileNetV2(nn.Module):
    def __init__(self, input_size=224, n_class=1000, width_mult=1., expand_ratio=6):
        super(MobileNetV2, self).__init__()
        block = InvertedResidual
        input_channel = 32
        last_channel = 1280
        interverted_residual_setting = [
            # t, c, n, s
            [1, 16, 1, 1],
            [expand_ratio, 24, 2, 2],
            [expand_ratio, 32, 3, 2],
            [expand_ratio, 64, 4, 2],
            [expand_ratio, 96, 3, 1],
            [expand_ratio, 160, 3, 2],
            [expand_ratio, 320, 1, 1],
        ]

        # building first layer
        assert input_size % 32 == 0
        input_channel = int(input_channel * width_mult)
        self.last_channel = int(last_channel * width_mult) if width_mult > 1.0 else last_channel
        self.features = [conv_bn(3, input_channel, 2)]
        # building inverted residual blocks
        for t, c, n, s in interverted_residual_setting:
            output_channel = int(c * width_mult)
            for i in range(n):
                if i == 0:
                    self.features.append(block(input_channel, output_channel, s, expand_ratio=t))
                else:
                    self.features.append(block(input_channel, output_channel, 1, expand_ratio=t))
                input_channel = output_channel
        # building last several layers
        self.features.append(conv_1x1_bn(input_channel, self.last_channel))
        # make it nn.Sequential
        self.features = nn.Sequential(*self.features)

        # building classifier
        self.classifier = nn.Sequential(
            nn.Dropout(0.2),
            nn.Linear(self.last_channel, n_class),
        )

        # build validator
        self.validator = AngleLinear(self.last_channel, n_class)

        self._initialize_weights()

    def forward(self, x, task='class'):
        """
        Implements forward pass

        Args:
            task (str): one of ['class', 'valid', 'feat']
        """
        x = self.features(x)
        x = x.mean(3).mean(2)

        if task == 'class':
            return self.classifier(x)
        elif task == 'valid':
            return self.validator(x)
        elif task == 'feat':
            return x
        else:
            raise ValueError("Result type not recognized.")

    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
                if m.bias is not None:
                    m.bias.data.zero_()
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()
            elif isinstance(m, nn.Linear):
                n = m.weight.size(1)
                m.weight.data.normal_(0, 0.01)
                m.bias.data.zero_()


class ResNet(nn.Module):
    def __init__(self, input_size=224, n_class=2300):
        super().__init__()

        assert input_size == 224
        self.model = torchvision.models.resnet34(pretrained=True)
        num_feats = self.model.fc.in_features
        self.model.fc = nn.Linear(num_feats, n_class)

    def forward(self, x):
        return self.model(x)


class CustomNet(nn.Module):
    """
    Custom architecture
        - adapts for unknown input into fully connected block
    """

    def __init__(self, input_shape, output_dim):
        """

        Args:
            input_shape (tuple): (channels, width, height)
            output_dim (int): dim of output
        """
        super().__init__()

        self.in_dim, _, _ = input_shape
        self.out_dim = output_dim

        self.conv = nn.Sequential(
            nn.BatchNorm2d(self.in_dim),
            nn.Conv2d(self.in_dim, 16, 3),
            nn.ReLU(),

            nn.Conv2d(16, 32, 3),
            nn.ReLU(),

            nn.Dropout2d(0.2),
            nn.Conv2d(32, 64, 5),
            nn.ReLU()
        )

        n_size = self._get_conv_output(input_shape)
        print(f"[DEBUG] Conv out dimension: {n_size}")

        self.fc = nn.Sequential(
            nn.Dropout(0.3),
            nn.Linear(n_size, 256),
            nn.ReLU(),

            nn.Dropout(0.3),
            nn.Linear(256, 128),
            nn.ReLU(),

            nn.Linear(128, self.out_dim)
        )

    def _get_conv_output(self, shape):
        """
        Get conv output shape
        """
        batch_size = 1
        input = Variable(torch.rand(batch_size, *shape))
        output_feat = self.conv(input)
        n_size = output_feat.data.view(batch_size, -1).size(1)
        return n_size

    def forward(self, x):
        x = self.conv(x)
        x = x.view(x.size(0), -1)
        x = self.fc(x)
        return x


class CustomNetV2(nn.Module):
    """
    Custom architecture
        - adapts for unknown input into fully connected block
    """

    def __init__(self, input_shape, output_dim):
        """

        Args:
            input_shape (tuple): (channels, width, height)
            output_dim (int): dim of output
        """
        super().__init__()

        self.in_dim, _, _ = input_shape
        self.out_dim = output_dim

        self.conv = nn.Sequential(
            nn.BatchNorm2d(self.in_dim),
            nn.Conv2d(self.in_dim, 16, 5, bias=False),
            nn.ReLU(),

            nn.Conv2d(16, 32, 5, bias=False),
            nn.ReLU(),

            nn.Dropout2d(0.2),
            nn.Conv2d(32, 32, 3, bias=False),
            nn.BatchNorm2d(32),
            nn.ReLU(),

            nn.MaxPool2d(2, 2),
            nn.Dropout2d(0.2),
            nn.Conv2d(32, 16, 3, bias=False),
            nn.ReLU()
        )

        n_size = self._get_conv_output(input_shape)
        print(f"[DEBUG] Conv out dimension: {n_size}")

        self.fc = nn.Sequential(
            nn.Dropout(0.3),
            nn.Linear(n_size, self.out_dim, bias=False),
        )

    def _get_conv_output(self, shape):
        """
        Get conv output shape
        """
        batch_size = 1
        input = Variable(torch.rand(batch_size, *shape))
        output_feat = self.conv(input)
        n_size = output_feat.data.view(batch_size, -1).size(1)
        return n_size

    def forward(self, x):
        x = self.conv(x)
        x = x.view(x.size(0), -1)
        x = self.fc(x)
        return x


class LeNet(nn.Module):
    """
    Famous LeNet-5 with some updates
    """

    def __init__(self, input_shape, out_dim):
        super(LeNet, self).__init__()

        self.convnet = nn.Sequential(
            nn.Conv2d(input_shape[0], 6, 5),
            nn.ReLU(),

            nn.MaxPool2d(2, 2),
            nn.Conv2d(6, 16, 5),
            nn.ReLU(),

            nn.MaxPool2d(2, 2),
            nn.Conv2d(16, 120, 5),
            nn.ReLU()
        )

        self.fc = nn.Sequential(
            nn.Linear(120, 256),
            nn.ReLU(),
            nn.Linear(256, 256),
            nn.ReLU(),
            nn.Linear(256, out_dim)
        )

    def forward(self, img):
        output = self.convnet(img)
        output = output.view(img.size(0), -1)
        output = self.fc(output)
        return output


def init_weights(m):
    if type(m) == nn.Conv2d or type(m) == nn.Linear:
        nn.init.xavier_normal_(m.weight.data)


def get_trainable_parameters(model):
    return [p.numel() for p in model.parameters() if p.requires_grad]


class SphereFace(nn.Module):
    def __init__(self, n_class=10574, feature=False):
        super(SphereFace, self).__init__()
        self.n_class = n_class
        self.feature = feature
        # input = B*3*112*96
        self.conv1_1 = nn.Conv2d(3, 64, 3, 2, 1)  # =>B*64*56*48
        self.relu1_1 = nn.PReLU(64)
        self.conv1_2 = nn.Conv2d(64, 64, 3, 1, 1)
        self.relu1_2 = nn.PReLU(64)
        self.conv1_3 = nn.Conv2d(64, 64, 3, 1, 1)
        self.relu1_3 = nn.PReLU(64)

        self.conv2_1 = nn.Conv2d(64, 128, 3, 2, 1)  # =>B*128*28*24
        self.relu2_1 = nn.PReLU(128)
        self.conv2_2 = nn.Conv2d(128, 128, 3, 1, 1)
        self.relu2_2 = nn.PReLU(128)
        self.conv2_3 = nn.Conv2d(128, 128, 3, 1, 1)
        self.relu2_3 = nn.PReLU(128)

        self.conv2_4 = nn.Conv2d(128, 128, 3, 1, 1)  # =>B*128*28*24
        self.relu2_4 = nn.PReLU(128)
        self.conv2_5 = nn.Conv2d(128, 128, 3, 1, 1)
        self.relu2_5 = nn.PReLU(128)

        self.conv3_1 = nn.Conv2d(128, 256, 3, 2, 1)  # =>B*256*14*12
        self.relu3_1 = nn.PReLU(256)
        self.conv3_2 = nn.Conv2d(256, 256, 3, 1, 1)
        self.relu3_2 = nn.PReLU(256)
        self.conv3_3 = nn.Conv2d(256, 256, 3, 1, 1)
        self.relu3_3 = nn.PReLU(256)

        self.conv3_4 = nn.Conv2d(256, 256, 3, 1, 1)  # =>B*256*14*12
        self.relu3_4 = nn.PReLU(256)
        self.conv3_5 = nn.Conv2d(256, 256, 3, 1, 1)
        self.relu3_5 = nn.PReLU(256)

        self.conv3_6 = nn.Conv2d(256, 256, 3, 1, 1)  # =>B*256*14*12
        self.relu3_6 = nn.PReLU(256)
        self.conv3_7 = nn.Conv2d(256, 256, 3, 1, 1)
        self.relu3_7 = nn.PReLU(256)

        self.conv3_8 = nn.Conv2d(256, 256, 3, 1, 1)  # =>B*256*14*12
        self.relu3_8 = nn.PReLU(256)
        self.conv3_9 = nn.Conv2d(256, 256, 3, 1, 1)
        self.relu3_9 = nn.PReLU(256)

        self.conv4_1 = nn.Conv2d(256, 512, 3, 2, 1)  # =>B*512*7*6
        self.relu4_1 = nn.PReLU(512)
        self.conv4_2 = nn.Conv2d(512, 512, 3, 1, 1)
        self.relu4_2 = nn.PReLU(512)
        self.conv4_3 = nn.Conv2d(512, 512, 3, 1, 1)
        self.relu4_3 = nn.PReLU(512)

        self.fc5 = nn.Linear(512 * 7 * 6, 512)
        self.fc6 = AngleLinear(512, self.n_class)

    def forward(self, x):
        x = self.relu1_1(self.conv1_1(x))
        x = x + self.relu1_3(self.conv1_3(self.relu1_2(self.conv1_2(x))))

        x = self.relu2_1(self.conv2_1(x))
        x = x + self.relu2_3(self.conv2_3(self.relu2_2(self.conv2_2(x))))
        x = x + self.relu2_5(self.conv2_5(self.relu2_4(self.conv2_4(x))))

        x = self.relu3_1(self.conv3_1(x))
        x = x + self.relu3_3(self.conv3_3(self.relu3_2(self.conv3_2(x))))
        x = x + self.relu3_5(self.conv3_5(self.relu3_4(self.conv3_4(x))))
        x = x + self.relu3_7(self.conv3_7(self.relu3_6(self.conv3_6(x))))
        x = x + self.relu3_9(self.conv3_9(self.relu3_8(self.conv3_8(x))))

        x = self.relu4_1(self.conv4_1(x))
        x = x + self.relu4_3(self.conv4_3(self.relu4_2(self.conv4_2(x))))

        x = x.view(x.size(0), -1)
        x = self.fc5(x)
        if self.feature:
            return x

        x = self.fc6(x)
        return x


class AngleLoss(nn.Module):
    def __init__(self, gamma=0):
        super(AngleLoss, self).__init__()
        self.gamma = gamma
        self.it = 0
        self.LambdaMin = 5.0
        self.LambdaMax = 1500.0
        self.lamb = 1500.0

    def forward(self, input, target):
        # fs - in_features / vector of features
        # cl - out_features / classes
        # bs - batch_size

        self.it += 1
        cos_theta, phi_theta = input
        target = target.view(-1, 1)  # size=(bs, 1)

        index = cos_theta.data * 0.0  # size=(bs, cl)
        index.scatter_(1, target.data.view(-1, 1), 1)
        index = index.byte()
        index = Variable(index)

        self.lamb = max(self.LambdaMin, self.LambdaMax / (1 + 0.1 * self.it))
        output = cos_theta * 1.0  # size=(bs, cl)
        output[index] -= cos_theta[index] * (1.0 + 0) / (1 + self.lamb)
        output[index] += phi_theta[index] * (1.0 + 0) / (1 + self.lamb)

        logpt = F.log_softmax(output)
        logpt = logpt.gather(1, target)
        logpt = logpt.view(-1)
        pt = Variable(logpt.data.exp())

        loss = -1 * (1 - pt) ** self.gamma * logpt
        loss = loss.mean()

        return loss


def lambda_x0(x):
    return x ** 0,


def lambda_x1(x):
    return x ** 1


def lambda_x2(x):
    return 2 * x ** 2 - 1


def lambda_x3(x):
    return 4 * x ** 3 - 3 * x


def lambda_x4(x):
    return 8 * x ** 4 - 8 * x ** 2 + 1


def lambda_x5(x):
    return 16 * x ** 5 - 20 * x ** 3 + 5 * x


class AngleLinear(nn.Module):
    def __init__(self, in_features, out_features, m=4, phiflag=True):
        super(AngleLinear, self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.weight = Parameter(torch.Tensor(in_features, out_features))
        self.weight.data.uniform_(-1, 1).renorm_(2, 1, 1e-5).mul_(1e5)
        self.phiflag = phiflag
        self.m = m
        self.mlambda = [
            lambda_x0,
            lambda_x1,
            lambda_x2,
            lambda_x3,
            lambda_x4,
            lambda_x5
        ]

    def forward(self, input):
        # ft - in_features / vector of features
        # cl - out_features / classes
        # bs - batch_size

        x = input  # size=(bs, fs)
        w = self.weight  # size=(fs, cl)

        ww = w.renorm(2, 1, 1e-5).mul(1e5)
        xlen = x.pow(2).sum(1).pow(0.5)  # size=(bs)
        wlen = ww.pow(2).sum(0).pow(0.5)  # size=(cl)

        cos_theta = x.mm(ww)  # size=(B,C)
        cos_theta = cos_theta / xlen.view(-1, 1) / wlen.view(1, -1)
        cos_theta = cos_theta.clamp(-1, 1)

        if self.phiflag:
            cos_m_theta = self.mlambda[self.m](cos_theta)
            theta = Variable(cos_theta.data.acos())
            k = (self.m * theta / 3.14159265).floor()
            n_one = k * 0.0 - 1
            phi_theta = (n_one ** k) * cos_m_theta - 2 * k
        else:
            theta = cos_theta.acos()
            phi_theta = myphi(theta, self.m)
            phi_theta = phi_theta.clamp(-1 * self.m, 1)

        cos_theta = cos_theta * xlen.view(-1, 1)  # size=(bs, cl)
        phi_theta = phi_theta * xlen.view(-1, 1)  # size=(bs, cl)

        return cos_theta, phi_theta


def myphi(x, m):
    x = x * m
    return 1 - x ** 2 / math.factorial(2) + x ** 4 / math.factorial(4) - x ** 6 / math.factorial(6) + \
           x ** 8 / math.factorial(8) - x ** 9 / math.factorial(9)
